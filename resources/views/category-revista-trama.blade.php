@extends('layouts.app')

@section('content')

@include('partials.category.banner1')
<!--==================GRUPO OilGas===============================-->
@include('partials.category-trama.group-cabecera')
<!--==================GRUPO OilGas===============================-->
<!--==================GRUPO Actualidad===============================-->
<section id="grupoActualidad" class="margen-superior-20">
  <div class="row">
  
    @include('partials.category.group-lista')

    <div class="col-12 col-lg-4">
      @include('partials.category.banner3')
    </div>
  </div>

  @if (App\has_banner('category', 'wide-3'))
  <div class="row justify-content-center linea-separadora-superior-completa-negra linea-separadora-inferior-completa-negra con-margen-inferior d-none d-lg-flex">
    <div class="banner-horizontal banner">
      {!! App\get_banners('category', 'wide-3') !!}
    </div>
  </div>
  @endif
</section>
<!--==================GRUPO actualidad===============================-->
<!--==================FORMULARIO ANUNCIANTES============================-->
@include('partials.assets.form-anunciantes')

@endsection
