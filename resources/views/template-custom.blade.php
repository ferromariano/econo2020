{{--
  Template Name: wide theme
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <section id="contenido-unico">
    <div class="row linea-separadora-superior-completa-negra linea-separadora-inferior-completa-negra">
      <div class="col-12">
        <article class="noticia con-margen-superior con-margen-inferior">
          <header><div class="noticia-titulo"><h1>{!! App::title() !!}</h1></div></header>
          <div class="noticia-social">
            <div class="nav-social-container">
              <div id="nav-social">
                <ul>
                  <li><a href="#" target="_blank"><span class="fa-stack" style="vertical-align: top;"><i class="fab fa-whatsapp fa-stack-2x"></i></span></a></li><li><a href="https://web.facebook.com/econojournal/" target="_blank"><span class="fa-stack" style="vertical-align: top;"><i class="far fa-circle fa-stack-2x"></i><i class="fab fa-facebook-f fa-stack-1x"></i></span></a></li>
                  <li><a href="https://twitter.com/econojournal" target="_blank"><span class="fa-stack" style="vertical-align: top;"><i class="far fa-circle fa-stack-2x"></i><i class="fab fa-twitter fa-stack-1x"></i></span></a></li>
                  <li><a href="https://ar.linkedin.com/company/econojournal.com.ar" target="_blank"><span class="fa-stack" style="vertical-align: top;"><i class="far fa-circle fa-stack-2x"></i><i class="fab fa-linkedin-in fa-stack-1x"></i></span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </article>
      </div>
    </div>
  </section>
  <section id="contacto-newsletter">
    <div class="row">
      <div class="col">
        @php 
          the_content() 
        @endphp
      </div>
    </div>
  </section>
  @endwhile
@endsection
