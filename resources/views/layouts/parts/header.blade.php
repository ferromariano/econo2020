<header class="header">
	<div class="clearfix navTop">
		<div class="container">
			<div class="wrapperBox">
				<div id="HeaderWtiPetroleo" class="header-indicator">
					<script type="text/javascript" src="https://www.oil-price.net/TABLE3/gen.php?lang=es"></script>
				</div>
				<div id="HeaderBrentPetroleo" class="header-indicator">
					<script type="text/javascript" src="https://www.oil-price.net/widgets/brent_text/gen.php?lang=es"></script>
				</div>
				<div id="HeaderGas" class="header-indicator">
					<script type="text/javascript" src="https://www.oil-price.net/widgets/natural_gas_text/gen.php?lang=es"></script>
				</div>
			</div>
		</div>
	</div>
	<div class="header-nav-logo">
		<div class="container">
			<div id="header-superior" class="row">
				<div class="header-logo-container">
					<a class="header-logo" href="{{ home_url('/') }}">
						<img class="logo" src="{!! App\asset_path('images/logo_header_negro_160.jpg') !!}" alt="{{ get_bloginfo('name', 'display') }}" title="{{ get_bloginfo('name', 'display') }}">
					</a>
				</div>
				<div class="menu-social-container">
					<div class="social-search-container">
						<div class="nav-social-container">
							<div id="nav-social-960">
								<ul>
									@include('partials.assets.layout.social')
								</ul>
							</div>
						</div>
						<div class="search-container">
							<form role="search" method="get" class="search-form" action="{!! esc_url( home_url( '/' ) ) !!}">
								<a href="{!! App::pageLinkNewsletter() !!}">
									<span class="lblSuscribite">Suscribite</span>
								</a>
								<div class="search">
									<i class="fas fa-search"></i>
									<input name="s" type="search" class="search-header" value="{!! get_search_query() !!}" placeholder="BUSCAR EN ECONOJOURNAL">
								</div>
							</form>
						</div>
					</div>
					<div class="nav-menu-container">
						<nav id="main-nav" class="navbar navbar-expand-md">
							@if (has_nav_menu('primary_navigation'))
							{!! wp_nav_menu(['theme_location' => 'primary_navigation', 
															 'container_class'=> 'collapse navbar-collapse',
															 'container_id'   => 'navbarSupportedContent',
															 'menu_class'     => 'menu navbar-nav mr-auto' ]) !!}
							@endif
						</nav><!-- /#main-nav -->
					</div>
				</div>
				<div class="col-mobile-menu">
					<a href="{!! App::pageLinkNewsletter() !!}">
						<span class="lblSuscribite">Suscribite</span>
					</a>
					<div class="navbar navbar-dark">
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar4" aria-controls="collapsingNavbar4" aria-expanded="false" aria-label="Toggle navigation">
              <i class="fas fa-bars"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="navMobile">
  	<div class="container">
  		<div class="row">
  			<div class="col-12 col-sm-6 offset-sm-6 px-0">
					@if (has_nav_menu('primary_navigation'))
					{!! wp_nav_menu(['theme_location' => 'primary_navigation', 
													 'container_class'=> 'navbar-collapse collapse',
													 'container_id'   => 'collapsingNavbar4',
													 'menu_class'     => 'menu navbar-nav mr-auto' ]) !!}
					@endif
  			</div>
  		</div>
  	</div>
  </div>
  <div class="mobile-social-search-container">
  	<div class="container">
  		<div class="social-search-container row">
  			<div class="nav-social-container col-auto">
  				<div id="nav-social-720">
  					<ul>
  						@include('partials.assets.layout.social')
  					</ul>
  				</div>
  			</div>
  			<div class="search-container col">
  				@include('partials.assets.layout.form-search')
  			</div>
  		</div>
  	</div>
  </div>
</header>