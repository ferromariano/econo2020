<a class="scroll-to-top" id="scrollToTop">
    <!--<i class="fas fa-angle-up"></i>-->
</a>
<footer>
    <div class="container">
        <div class="row">
            <div class="footer-logo-container">
                <a class="footer-logo" href="index.html">
                    <img src="{!! App\asset_path('images/logo_footer_190.png') !!}" alt="logo footer" title="logo footer">
                </a>
            </div>
            <div class="copyright-container order-3 order-md-2">
                <p>© ECONOJOURNAL</p>
                <p>Primera agencia de noticias de Oil&Gas, Energía y Minería. Contenidos 100% propios.</p>
                <p>Periodismo especializado. Proyectos e inversiones en Vaca Muerta.</p>
            </div>
            <div class="nav-social-container order-2 order-md-3">
                <div id="nav-social-footer">
                    <ul>
                        <li>
                            <a href="#" target="_blank">
                                <span class="fa-stack" style="vertical-align: top;">
                                    <i class="fab fa-whatsapp fa-stack-2x"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="https://web.facebook.com/econojournal/" target="_blank">
                                <span class="fa-stack" style="vertical-align: top;">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-facebook-f fa-stack-1x"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/econojournal/" target="_blank">
                                <span class="fa-stack" style="vertical-align: top;">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-instagram fa-stack-1x"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCBbl2OHJCjOXh1yDQJ60ReQ" target="_blank">
                                <span class="fa-stack" style="vertical-align: top;">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-youtube fa-stack-1x"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/econojournal" target="_blank">
                                <span class="fa-stack" style="vertical-align: top;">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-twitter fa-stack-1x"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="https://ar.linkedin.com/company/econojournal.com.ar" target="_blank">
                                <span class="fa-stack" style="vertical-align: top;">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-linkedin-in fa-stack-1x"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank">
                                <span class="fa-stack" style="vertical-align: top;">
                                    <i class="far fa-circle fa-stack-2x"></i>
                                    <i class="fab fa-skype fa-stack-1x"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>