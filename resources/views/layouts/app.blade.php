<!doctype html>
<html {!! get_language_attributes() !!}>
@include('layouts.parts.head')
<body @php body_class() @endphp>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-92837493-1', 'auto');
    ga('send', 'pageview');

  </script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-158747076-1">
</script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-158747076-1');
</script>

  @php do_action('get_header') @endphp
  @include('layouts.parts.header')
  <main role="main">
    <div class="container">
      @yield('content')
    </div>
  </main>
  @php do_action('get_footer') @endphp
  @include('layouts.parts.footer')
  @php wp_footer() @endphp
  @yield('after_page')



<div data-popin="exampleModal32" class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <button style="    position: absolute;
top: 5vh;
right: 5vw;
color: #f00;
text-shadow: none;
opacity: 1;
font-size: 3rem;
background-color: #FFF;
border-radius: 73%;
width: 3rem;
height: 3rem;
line-height: 0rem;
text-align: center;" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="margin-top: -3px; display: block;">&times;</span></button>
    <a href="https://econojournal.com.ar/newsletter/" style="display: block;
    text-align: center;
    height: 100vh;
    line-height: 100vh;">
      <img class="logo" src="{!! App\asset_path('images/Lanzamiento-WEB_PopUp-1200x1200px.jpg') !!}" alt="{{ get_bloginfo('name', 'display') }}" title="{{ get_bloginfo('name', 'display') }}">
    </a>
</div>

</body>
</html>
