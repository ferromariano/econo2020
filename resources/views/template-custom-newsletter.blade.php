{{--
  Template Name: newsletter theme
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <section id="contenido-unico">
    <div class="row linea-separadora-superior-completa-negra linea-separadora-inferior-completa-negra">
      <div class="col-12">
        <article class="noticia con-margen-superior con-margen-inferior">
          <header><div class="noticia-titulo"><h1>{!! App::title() !!}</h1></div></header>
          <div class="noticia-texto">
            <p class="copete">{!! FrontPage::postContenido() !!}</p>
          </div>
          <div class="noticia-social">
            <div class="nav-social-container">
              <div id="nav-social">
                <ul>
                  <li><a href="#" target="_blank"><span class="fa-stack" style="vertical-align: top;"><i class="fab fa-whatsapp fa-stack-2x"></i></span></a></li><li><a href="https://web.facebook.com/econojournal/" target="_blank"><span class="fa-stack" style="vertical-align: top;"><i class="far fa-circle fa-stack-2x"></i><i class="fab fa-facebook-f fa-stack-1x"></i></span></a></li>
                  <li><a href="https://twitter.com/econojournal" target="_blank"><span class="fa-stack" style="vertical-align: top;"><i class="far fa-circle fa-stack-2x"></i><i class="fab fa-twitter fa-stack-1x"></i></span></a></li>
                  <li><a href="https://ar.linkedin.com/company/econojournal.com.ar" target="_blank"><span class="fa-stack" style="vertical-align: top;"><i class="far fa-circle fa-stack-2x"></i><i class="fab fa-linkedin-in fa-stack-1x"></i></span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </article>
      </div>
    </div>
  </section>
  <section id="contacto-newsletter">
    <div class="row">
      <div class="col">

        <div id="contacto-newsletter">
          <div class="form-container">
            <form id="mc-embedded-subscribe-form" class="validate" action="https://econojuornal.us13.list-manage.com/subscribe/post?u=4ea8e149f4d6757562142882a&amp;id=3dc38a1f4b" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">

              <div class="row">
                <div class="col-12 col-md-4"><div class="form-group"><label for="mce-EMAIL">Correo electronico <span class="asterisk">*</span></label><input id="mce-EMAIL" class="form-control required email" name="EMAIL" type="email" value=""></div></div>
                <div class="col-12 col-md-4"><div class="form-group"><label for="mce-MMERGE3">Empresa </label><input id="mce-MMERGE3" class="form-control" name="MMERGE3" type="text" value=""></div></div>
                <div class="col-12 col-md-4"><div class="form-group"><label for="mce-MMERGE4-addr1">Direccion </label><input id="mce-MMERGE4-addr1" class="form-control" maxlength="70" name="MMERGE4[addr1]" type="text" value=""></div></div>
                <div class="col-12 col-md-4"><div class="form-group"><label for="mce-MMERGE4-addr2">Direccion 2</label><input id="mce-MMERGE4-addr2" class="form-control" maxlength="70" name="MMERGE4[addr2]" type="text" value=""></div></div>
                <div class="col-12 col-md-4"><div class="form-group"><label for="mce-MMERGE4-city">Ciudad</label><input id="mce-MMERGE4-city" class="form-control" maxlength="40" name="MMERGE4[city]" type="text" value=""></div></div>
                <div class="col-12 col-md-4"><div class="form-group"><label for="mce-MMERGE4-state">Provincia</label><input id="mce-MMERGE4-state" class="form-control" maxlength="20" name="MMERGE4[state]" type="text" value=""></div></div>
                <div class="col-12 col-md-4"><div class="form-group"><label for="mce-MMERGE4-zip">Codigo postal</label><input id="mce-MMERGE4-zip" class="form-control" maxlength="10" name="MMERGE4[zip]" type="text" value=""></div></div>
                <div class="col-12 col-md-4"><div class="form-group"><label for="mce-MMERGE4-country">Pais</label><select id="mce-MMERGE4-country" class="form-control" name="MMERGE4[country]"><option value="164">USA</option><option value="286">Aaland Islands</option><option value="274">Afghanistan</option><option value="2">Albania</option><option value="3">Algeria</option><option value="178">American Samoa</option><option value="4">Andorra</option><option value="5">Angola</option><option value="176">Anguilla</option><option value="175">Antigua And Barbuda</option><option selected="selected" value="6">Argentina</option><option value="7">Armenia</option><option value="179">Aruba</option><option value="8">Australia</option><option value="9">Austria</option><option value="10">Azerbaijan</option><option value="11">Bahamas</option><option value="12">Bahrain</option><option value="13">Bangladesh</option><option value="14">Barbados</option><option value="15">Belarus</option><option value="16">Belgium</option><option value="17">Belize</option><option value="18">Benin</option><option value="19">Bermuda</option><option value="20">Bhutan</option><option value="21">Bolivia</option><option value="325">Bonaire, Saint Eustatius and Saba</option><option value="22">Bosnia and Herzegovina</option><option value="23">Botswana</option><option value="181">Bouvet Island</option><option value="24">Brazil</option><option value="180">Brunei Darussalam</option><option value="25">Bulgaria</option><option value="26">Burkina Faso</option><option value="27">Burundi</option><option value="28">Cambodia</option><option value="29">Cameroon</option><option value="30">Canada</option><option value="31">Cape Verde</option><option value="32">Cayman Islands</option><option value="33">Central African Republic</option><option value="34">Chad</option><option value="35">Chile</option><option value="36">China</option><option value="185">Christmas Island</option><option value="37">Colombia</option><option value="204">Comoros</option><option value="38">Congo</option><option value="183">Cook Islands</option><option value="268">Costa Rica</option><option value="275">Cote D&#8217;Ivoire</option><option value="40">Croatia</option><option value="276">Cuba</option><option value="298">Curacao</option><option value="41">Cyprus</option><option value="42">Czech Republic</option><option value="318">Democratic Republic of the Congo</option><option value="43">Denmark</option><option value="44">Djibouti</option><option value="289">Dominica</option><option value="187">Dominican Republic</option><option value="45">Ecuador</option><option value="46">Egypt</option><option value="47">El Salvador</option><option value="48">Equatorial Guinea</option><option value="49">Eritrea</option><option value="50">Estonia</option><option value="51">Ethiopia</option><option value="189">Falkland Islands</option><option value="191">Faroe Islands</option><option value="52">Fiji</option><option value="53">Finland</option><option value="54">France</option><option value="193">French Guiana</option><option value="277">French Polynesia</option><option value="56">Gabon</option><option value="57">Gambia</option><option value="58">Georgia</option><option value="59">Germany</option><option value="60">Ghana</option><option value="194">Gibraltar</option><option value="61">Greece</option><option value="195">Greenland</option><option value="192">Grenada</option><option value="196">Guadeloupe</option><option value="62">Guam</option><option value="198">Guatemala</option><option value="270">Guernsey</option><option value="63">Guinea</option><option value="65">Guyana</option><option value="200">Haiti</option><option value="66">Honduras</option><option value="67">Hong Kong</option><option value="68">Hungary</option><option value="69">Iceland</option><option value="70">India</option><option value="71">Indonesia</option><option value="278">Iran</option><option value="279">Iraq</option><option value="74">Ireland</option><option value="323">Isle of Man</option><option value="75">Israel</option><option value="76">Italy</option><option value="202">Jamaica</option><option value="78">Japan</option><option value="288">Jersey (Channel Islands)</option><option value="79">Jordan</option><option value="80">Kazakhstan</option><option value="81">Kenya</option><option value="203">Kiribati</option><option value="82">Kuwait</option><option value="83">Kyrgyzstan</option><option value="84">Lao People&#8217;s Democratic Republic</option><option value="85">Latvia</option><option value="86">Lebanon</option><option value="87">Lesotho</option><option value="88">Liberia</option><option value="281">Libya</option><option value="90">Liechtenstein</option><option value="91">Lithuania</option><option value="92">Luxembourg</option><option value="208">Macau</option><option value="93">Macedonia</option><option value="94">Madagascar</option><option value="95">Malawi</option><option value="96">Malaysia</option><option value="97">Maldives</option><option value="98">Mali</option><option value="99">Malta</option><option value="207">Marshall Islands</option><option value="210">Martinique</option><option value="100">Mauritania</option><option value="212">Mauritius</option><option value="241">Mayotte</option><option value="101">Mexico</option><option value="102">Moldova, Republic of</option><option value="103">Monaco</option><option value="104">Mongolia</option><option value="290">Montenegro</option><option value="294">Montserrat</option><option value="105">Morocco</option><option value="106">Mozambique</option><option value="242">Myanmar</option><option value="107">Namibia</option><option value="108">Nepal</option><option value="109">Netherlands</option><option value="110">Netherlands Antilles</option><option value="213">New Caledonia</option><option value="111">New Zealand</option><option value="112">Nicaragua</option><option value="113">Niger</option><option value="114">Nigeria</option><option value="217">Niue</option><option value="214">Norfolk Island</option><option value="272">North Korea</option><option value="116">Norway</option><option value="117">Oman</option><option value="118">Pakistan</option><option value="222">Palau</option><option value="282">Palestine</option><option value="119">Panama</option><option value="219">Papua New Guinea</option><option value="120">Paraguay</option><option value="121">Peru</option><option value="122">Philippines</option><option value="221">Pitcairn</option><option value="123">Poland</option><option value="124">Portugal</option><option value="126">Qatar</option><option value="315">Republic of Kosovo</option><option value="127">Reunion</option><option value="128">Romania</option><option value="129">Russia</option><option value="130">Rwanda</option><option value="205">Saint Kitts and Nevis</option><option value="206">Saint Lucia</option><option value="324">Saint Martin</option><option value="237">Saint Vincent and the Grenadines</option><option value="132">Samoa (Independent)</option><option value="227">San Marino</option><option value="255">Sao Tome and Principe</option><option value="133">Saudi Arabia</option><option value="134">Senegal</option><option value="326">Serbia</option><option value="135">Seychelles</option><option value="136">Sierra Leone</option><option value="137">Singapore</option><option value="302">Sint Maarten</option><option value="138">Slovakia</option><option value="139">Slovenia</option><option value="223">Solomon Islands</option><option value="140">Somalia</option><option value="141">South Africa</option><option value="257">South Georgia and the South Sandwich Islands</option><option value="142">South Korea</option><option value="311">South Sudan</option><option value="143">Spain</option><option value="144">Sri Lanka</option><option value="293">Sudan</option><option value="146">Suriname</option><option value="225">Svalbard and Jan Mayen Islands</option><option value="147">Swaziland</option><option value="148">Sweden</option><option value="149">Switzerland</option><option value="285">Syria</option><option value="152">Taiwan</option><option value="260">Tajikistan</option><option value="153">Tanzania</option><option value="154">Thailand</option><option value="233">Timor-Leste</option><option value="155">Togo</option><option value="232">Tonga</option><option value="234">Trinidad and Tobago</option><option value="156">Tunisia</option><option value="157">Turkey</option><option value="158">Turkmenistan</option><option value="287">Turks &amp; Caicos Islands</option><option value="159">Uganda</option><option value="161">Ukraine</option><option value="162">United Arab Emirates</option><option value="262">United Kingdom</option><option value="163">Uruguay</option><option value="165">Uzbekistan</option><option value="239">Vanuatu</option><option value="166">Vatican City State (Holy See)</option><option value="167">Venezuela</option><option value="168">Vietnam</option><option value="169">Virgin Islands (British)</option><option value="238">Virgin Islands (U.S.)</option><option value="188">Western Sahara</option><option value="170">Yemen</option><option value="173">Zambia</option><option value="174">Zimbabwe</option></select></div></div>
                <div class="col-12 col-md-4">
                  <div class="form-check">
                    <input class="form-check-input" id="mce-EMAILTYPE-0" name="EMAILTYPE" type="radio" value="html">
                    <label class="form-check-label" for="mce-EMAILTYPE-0"> Formato HTML</label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" id="mce-EMAILTYPE-1" name="EMAILTYPE" type="radio" value="text">
                    <label class="form-check-label" for="mce-EMAILTYPE-1"> Formato Texto</label>
                  </div>
                </div>
              </div>

              <div id="mce-responses" class="clear">
                <div id="mce-error-response" class="response" style="display: none;"></div>
                <div id="mce-success-response" class="response" style="display: none;"></div>
              </div>

              <div style="position: absolute; left: -5000px;" aria-hidden="true"><input tabindex="-1" name="b_4ea8e149f4d6757562142882a_3dc38a1f4b" type="text" value=""></div>
              <div class="clear"><input id="mc-embedded-subscribe" class="button btn btn-success btn-block btn-lg" name="subscribe" type="submit" value="Subscribe"></div>

            </form>
          </div>
        </div>


      </div>
    </div>
  </section>
  @endwhile
@endsection


@section('after_page')
<script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js"></script>
<script type="text/javascript">
  (function($) {
    window.fnames = new Array(); 
    window.ftypes = new Array();
    fnames[0]='EMAIL';
    ftypes[0]='email';
    fnames[1]='FNAME';
    ftypes[1]='text';
    fnames[2]='LNAME';
    ftypes[2]='text';
    fnames[3]='MMERGE3';
    ftypes[3]='text';
    fnames[4]='MMERGE4';
    ftypes[4]='address';
    $.extend($.validator.messages, {
      required:     "Este campo es obligatorio.",
      remote:       "Por favor, rellena este campo.",
      email:        "Por favor, escribe una dirección de correo válida",
      url:          "Por favor, escribe una URL válida.",
      date:         "Por favor, escribe una fecha válida.",
      dateISO:      "Por favor, escribe una fecha (ISO) válida.",
      number:       "Por favor, escribe un número entero válido.",
      digits:       "Por favor, escribe sólo dígitos.",
      creditcard:   "Por favor, escribe un número de tarjeta válido.",
      equalTo:      "Por favor, escribe el mismo valor de nuevo.",
      accept:       "Por favor, escribe un valor con una extensión aceptada.",
      maxlength:   $.validator.format("Por favor, no escribas más de {0} caracteres."),
      minlength:   $.validator.format("Por favor, no escribas menos de {0} caracteres."),
      rangelength: $.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
      range:       $.validator.format("Por favor, escribe un valor entre {0} y {1}."),
      max:         $.validator.format("Por favor, escribe un valor menor o igual a {0}."),
      min:         $.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
    });}(jQuery));
  var $mcj = jQuery.noConflict(true);
</script>

<style type="text/css">
  #mc_embed_signup{ font: 14px Helvetica,Arial,sans-serif; padding: 1px 20px 10px 20px; }
</style>

@endsection

