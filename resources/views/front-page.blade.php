@extends('layouts.app')

@section('content')
  @include('partials.home.headlines')
  <!--==================GRUPO OilGas===============================-->
  @include('partials.home.oilgas')
  <!--==================GRUPO OilGas===============================-->
  <!--==================GRUPO Energia===============================-->
  @include('partials.home.energia')
  <!--==================GRUPO Energia===============================-->
  <!--==================GRUPO Renovables===============================-->
  @include('partials.home.renovables')
  <!--==================GRUPO Renovables===============================-->
  <!--==================GRUPO Actualidad===============================-->
  @include('partials.home.actualidad')
  <!--==================GRUPO actualidad===============================-->
  <!--==================GRUPO Videos Destacados===============================-->
  @include('partials.home.videos')
  <!--==================GRUPO Videos destacados===============================-->
  <!--==================GRUPO RevistaTrama===============================-->
  @include('partials.home.trama')
  <!--==================GRUPO RevistaTrama===============================-->
  <!--==================GRUPO Mineria===============================-->
  @include('partials.home.mineria')
  <!--==================GRUPO Mineria===============================-->
  <!--==================GRUPO Opinión===============================-->
  @include('partials.home.opinion')
  <!--==================GRUPO Opinión===============================-->
  <!--==================FORMULARIO============================-->
  @include('partials.assets.form-anunciantes')
@endsection
