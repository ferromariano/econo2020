  <section id="grupoRenovables" class="margen-superior-20">
    <div class="sin-margen-izquierdo sin-margen-derecho linea-separadora-superior-completa-negra subtitulo-grupo-noticias-container">
      <div class="subtitulo-grupo-noticias">
        <img src="{!! App\asset_path('images/headlines_back.jpg') !!}" alt="headlines_back" title="headlines_back" />
        <div class="texto-centrado">
          <h2>Renovables</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-lg-8">
        <div class="row">
          <div class="col-12">
            @if (FrontPage::havePost('renovables'))
            <article class="noticia-margen-sidebar-izquierdo noticia noticia-destacada con-margen-inferior">
              <div class="row">
                <div class="col-12 col-md-6">
                  <header>
                    <div class="noticia-titulo">
                      <a href="{!! FrontPage::postLink() !!}">
                        <h3>{!! FrontPage::postTitle() !!}</h3>
                      </a>
                    </div>
                  </header>
                  <div class="noticia-texto con-margen-inferior">{!! FrontPage::postContenido() !!}</div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="noticia-imagen">
                    <a href="{!! FrontPage::postLink() !!}">
                      <figure>
                        {!! FrontPage::postImagenHome('548×247') !!}
                      </figure>
                    </a>
                  </div>
                </div>
              </div>
            </article>
            @php
            FrontPage::resetPost()
            @endphp
            @endif
          </div>
          <div class="col-12">
            <div class="noticia-margen-sidebar-izquierdo">
              <div class="row">
                <div class="col-12 col-md-6 linea-separadora-derecha-completa-gris">
                  @if (FrontPage::havePost('renovables'))
                  <article class="noticia noticia-vertical noticia-vertical-completa con-margen-inferior">
                    <div class="noticia-imagen">
                      <a href="{!! FrontPage::postLink() !!}">
                        <figure>
                          {!! FrontPage::postImagenHome('547×274') !!}
                        </figure>
                      </a>
                    </div>
                    <header>
                      <div class="noticia-volanta">{!! FrontPage::postVolanta() !!}</div>
                      <div class="noticia-titulo">
                        <a href="{!! FrontPage::postLink() !!}">
                          <h3>{!! FrontPage::postTitle() !!}</h3>
                        </a>
                      </div>
                    </header>
                    <div class="noticia-texto">{!! FrontPage::postContenido() !!}</div>
                  </article>
                  @php
                  FrontPage::resetPost()
                  @endphp
                  @endif
                </div>
                <div class="col-12 col-md-6">
                  @if (FrontPage::havePost('renovables'))
                  <article class="noticia noticia-destacada con-margen-inferior">
                    <div class="separator"></div>
                    <header>
                      <div class="noticia-titulo">
                        <a href="{!! FrontPage::postLink() !!}">
                          <h3>{!! FrontPage::postTitle() !!}</h3>
                        </a>
                      </div>
                    </header>
                    <div class="noticia-texto">{!! FrontPage::postContenido() !!}</div>
                  </article>
                  @php
                  FrontPage::resetPost()
                  @endphp
                  @endif
                  @if (FrontPage::havePost('renovables'))
                  <article class="noticia noticia-destacada con-margen-inferior">
                    <div class="separator sin-margen-superior"></div>
                    <header>
                      <div class="noticia-volanta">{!! FrontPage::postVolanta() !!}</div>
                      <div class="noticia-titulo">
                        <a href="{!! FrontPage::postLink() !!}">
                          <h3>{!! FrontPage::postTitle() !!}</h3>
                        </a>
                      </div>
                    </header>
                    <div class="noticia-texto">{!! FrontPage::postContenido() !!}</div>
                  </article>
                  @php
                  FrontPage::resetPost()
                  @endphp
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-4">
        @if (App\has_banners('home', array('banner-10','banner-11','banner-12','banner-13',)))
        <div class="banner-vertical banner sin-margen-derecho">
          @if (App\has_banner('home', 'banner-10'))
          <div class="banner-vertical-1 centar-contenido">
            {!! App\get_banners('home', 'banner-10') !!}
          </div>
          <div class="w-100 d-md-none"></div>
          @endif
          @if (App\has_banner('home', 'banner-11'))
          <div class="banner-vertical-2 centar-contenido">
            {!! App\get_banners('home', 'banner-11') !!}
          </div>
          @endif
          @if (App\has_banner('home', 'banner-12'))
          <div class="banner-vertical-3 centar-contenido">
            {!! App\get_banners('home', 'banner-12') !!}
          </div>
          @endif
          @if (App\has_banner('home', 'banner-13'))
          <div class="banner-vertical-4 centar-contenido">
            {!! App\get_banners('home', 'banner-13') !!}
          </div>
          @endif
        </div>
        @endif
      </div>
    </div>
  </section>