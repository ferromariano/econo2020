  <section id="grupoEnergia" class="margen-superior-20">
    <div class="sin-margen-izquierdo sin-margen-derecho linea-separadora-superior-completa-negra subtitulo-grupo-noticias-container">
      <div class="subtitulo-grupo-noticias">
        <img src="{!! App\asset_path('images/headlines_back.jpg') !!}" alt="headlines_back" title="headlines_back" />
        <div class="texto-centrado">
          <h2>Energía</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-6 col-xl-7">
        <div class="row">
          <div class="col-12 col-xl-9">
            @if (FrontPage::havePost('energia'))
            <article class="noticia-margen-sidebar-izquierdo noticia noticia-destacada">
              <div class="noticia-imagen">
                <a href="{!! FrontPage::postLink() !!}">
                  <figure>
                    {!! FrontPage::postImagenHome('547×295') !!}
                  </figure>
                </a>
              </div>
              <header>
                <div class="noticia-titulo">
                  <a href="{!! FrontPage::postLink() !!}">
                    <h3>{!! FrontPage::postTitle() !!}</h3>
                  </a>
                </div>
              </header>
              <div class="noticia-texto">{!! FrontPage::postContenido() !!}</div>
            </article>
            @php
            FrontPage::resetPost()
            @endphp
            @endif
          </div>
          <div class="col-12 col-xl-3">
            <div class="noticias-verticales-agrupadas noticia-margen-sidebar-sololg-izquierdo con-margen-inferior">
              @if (FrontPage::havePost('energia'))
              <article class="noticia noticia-vertical">
                <div class="separator sin-margen-superior"></div>
                <div class="noticia-titulo">
                  <a href="{!! FrontPage::postLink() !!}">
                    <h3>{!! FrontPage::postTitle() !!}</h3>
                  </a>
                </div>
              </article>
              @php
              FrontPage::resetPost()
              @endphp
              @endif
              @if (FrontPage::havePost('energia'))
              <article class="noticia noticia-vertical ">
                <div class="separator"></div>
                <div class="noticia-titulo">
                  <a href="{!! FrontPage::postLink() !!}">
                    <h3>{!! FrontPage::postTitle() !!}</h3>
                  </a>
                </div>
              </article>
              @php
              FrontPage::resetPost()
              @endphp
              @endif
              @if (FrontPage::havePost('energia'))
              <article class="noticia noticia-vertical ">
                <div class="separator"></div>
                <div class="noticia-titulo">
                  <a href="{!! FrontPage::postLink() !!}">
                    <h3>{!! FrontPage::postTitle() !!}</h3>
                  </a>
                </div>
              </article>
              @php
              FrontPage::resetPost()
              @endphp
              @endif
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-xl-5">
        <div class="row">
          <div class="col-12 col-xl-5">
            <div class="noticia-vertical-unica con-margen-inferior">
              @if (FrontPage::havePost('energia'))
              <article class="noticia noticia-destacada">
                <div class="separator sin-margen-superior"></div>
                <header>
                  <div class="noticia-volanta">{!! FrontPage::postVolanta() !!}</div>
                  <div class="noticia-titulo">
                    <a href="{!! FrontPage::postLink() !!}">
                      <h3>{!! FrontPage::postTitle() !!}</h3>
                    </a>
                  </div>
                </header>
                <div class="noticia-texto">{!! FrontPage::postContenido() !!}</div>
              </article>
              @php
              FrontPage::resetPost()
              @endphp
              @endif
            </div>
          </div>
          <div class="col-12 col-xl-7">
            @if (FrontPage::havePost('energia'))
            <article class="noticia noticia-vertical noticia-vertical-completa con-margen-inferior">
              <div class="d-none d-md-block noticia-imagen">
                <a href="{!! FrontPage::postLink() !!}">
                  <figure>
                    {!! FrontPage::postImagenHome('545×364') !!}
                  </figure>
                </a>
              </div>
              <header>
                <div class="noticia-volanta">{!! FrontPage::postVolanta() !!}</div>
                <div class="noticia-titulo">
                  <a href="{!! FrontPage::postLink() !!}">
                    <h3>{!! FrontPage::postTitle() !!}</h3>
                  </a>
                </div>
              </header>
              <div class="noticia-texto">{!! FrontPage::postContenido() !!}</div>
            </article>
            @php
            FrontPage::resetPost()
            @endphp
            @endif
          </div>
        </div>
      </div>
    </div>
    @if (App\has_banner('home', 'banner-9'))
    <div class="row d-flex d-lg-none">
      <div class="col-12 d-lg-none">
        <div class="banner-vertical banner sin-margen-derecho">
          <div class="banner-vertical-1 centar-contenido">
            {!! App\get_banners('home', 'banner-9') !!}
          </div>
        </div>
      </div>
    </div>
    @endif
    @if (App\has_banner('home', 'wide-3'))
    <div class="row justify-content-center linea-separadora-superior-completa-negra d-none d-lg-flex">
      <div class="banner-horizontal banner">
        {!! App\get_banners('home', 'wide-3') !!}
      </div>
    </div>
    @endif

  </section>