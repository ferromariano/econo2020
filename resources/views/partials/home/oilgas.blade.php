<section id="grupoOilGas" class="margen-superior-20">
  <div class="sin-margen-izquierdo sin-margen-derecho linea-separadora-superior-completa-negra subtitulo-grupo-noticias-container">
    <div class="subtitulo-grupo-noticias">
      <img src="{!! App\asset_path('images/headlines_back.jpg') !!}" alt="headlines_back" title="headlines_back" />
      <div class="texto-centrado">
        <h2>Oil & Gas</h2>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12 col-xl-8">
      <div class="row">
        <div class="col-12 col-md-8 col-lg-9 col-xl-8 linea-separadora-derecha-completa-gris">
          <div class="row">
            <div class="col-12">
              @if (FrontPage::havePost('oilgas'))
              <article class="noticia-margen-sidebar-izquierdo noticia noticia-destacada">
                <header>
                  <div class="noticia-titulo">
                    <a href="{!! FrontPage::postLink() !!}">
                      <h3>{!! FrontPage::postTitle() !!}</h3>
                    </a>
                  </div>
                </header>
                <div class="noticia-texto texto-post-titulo">{!! FrontPage::postContenido() !!}</div>
                <div class="noticia-imagen">
                  <a href="{!! FrontPage::postLink() !!}">
                    <figure>
                      {!! FrontPage::postImagenHome('940×352') !!}
                    </figure>
                  </a>
                </div>
              </article>
              @php
              FrontPage::resetPost()
              @endphp
              @endif                
            </div>
            <!--======BLOQUE INFERIOR======-->
            <div class="col-12 col-md-7 col-lg-8">
              <div class="noticia-margen-sidebar-izquierdo noticia-container linea-separadora-derecha-completa-gris">
                @if (FrontPage::havePost('oilgas'))
                <article class="noticia noticia-destacada">
                  <div class="separator"></div>
                  <header>
                    <div class="noticia-titulo">
                      <a href="{!! FrontPage::postLink() !!}">
                        <h3>{!! FrontPage::postTitle() !!}</h3>
                      </a>
                    </div>
                  </header>
                  <div class="noticia-texto">{!! FrontPage::postContenido() !!}</div>
                </article>
                @php
                FrontPage::resetPost()
                @endphp
                @endif                  
              </div>
            </div>
            <div class="col-12 col-md-5 col-lg-4">
              @if (FrontPage::havePost('oilgas'))
              <article class="noticia noticia-vertical ">
                <div class="separator"></div>
                <div class="noticia-imagen">
                  <a href="{!! FrontPage::postLink() !!}">
                    <figure>
                      {!! FrontPage::postImagenHome('340×168') !!}
                    </figure>
                  </a>
                </div>
                <header>
                  <div class="noticia-titulo">
                    <a href="{!! FrontPage::postLink() !!}">
                      <h3>{!! FrontPage::postTitle() !!}</h3>
                    </a>
                  </div>
                </header>
              </article>
              @php
              FrontPage::resetPost()
              @endphp
              @endif                
            </div>
            <!--======BLOQUE INFERIOR===========-->
          </div>
        </div>
        <div class="col-12 col-md-4 col-lg-3 col-xl-4">
          @if (FrontPage::havePost('oilgas'))
          <article class="noticia noticia-vertical noticia-vertical-completa">
            <div class="d-none d-md-block noticia-imagen">
              <a href="{!! FrontPage::postLink() !!}">
                <figure>
                  {!! FrontPage::postImagenHome('540×296') !!}
                </figure>
              </a>
            </div>
            <header>
              <div class="noticia-volanta">{!! FrontPage::postVolanta() !!}</div>
              <div class="noticia-titulo"><a href="{!! FrontPage::postLink() !!}"><h3>{!! FrontPage::postTitle() !!}</h3></a></div>
            </header>
            <div class="noticia-texto">{!! FrontPage::postContenido() !!}</div>
            <div class="separator"></div>
          </article>
          @php
          FrontPage::resetPost()
          @endphp
          @endif            
          @if (FrontPage::havePost('oilgas'))
          <article class="noticia noticia-destacada anular-margen-izquierdo-992">
            <header>
              <div class="noticia-volanta">{!! FrontPage::postVolanta() !!}</div>
              <div class="noticia-titulo"><a href="{!! FrontPage::postLink() !!}"><h3>{!! FrontPage::postTitle() !!}</h3></a></div>
            </header>
            <div class="noticia-texto">{!! FrontPage::postContenido() !!}</div>
          </article>
          @php
          FrontPage::resetPost()
          @endphp
          @endif            
        </div>
      </div>
    </div>
    <div class="col-12 d-lg-none col-xl-4 d-xl-block">
      @if (App\has_banners( 'home', array('banner-5','banner-6','banner-7','banner-8') ))
      <div class="banner-vertical banner sin-margen-derecho">
        @if (App\has_banner('home', 'banner-5'))
        <div class="banner-vertical-1 centar-contenido">
          {!! App\get_banners('home', 'banner-5') !!}
        </div>
        @endif
        @if (App\has_banner('home', 'banner-6'))
        <div class="banner-vertical-2 centar-contenido">
          {!! App\get_banners('home', 'banner-6') !!}
        </div>
        @endif
        @if (App\has_banner('home', 'banner-7'))
        <div class="banner-vertical-3 centar-contenido">
          {!! App\get_banners('home', 'banner-7') !!}
        </div>
        @endif
        @if (App\has_banner('home', 'banner-8'))
        <div class="banner-vertical-4 centar-contenido">
          {!! App\get_banners('home', 'banner-8') !!}
        </div>
        @endif
      </div>
      @endif
    </div>
  </div>
  @if (App\has_banner('home', 'wide-2'))
  <div class="row justify-content-center">
    <div class="banner-horizontal banner">
      {!! App\get_banners('home', 'wide-2') !!}
    </div>
  </div>
  @endif
</section>