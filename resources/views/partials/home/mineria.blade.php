  <section id="grupoMineria" class="margen-superior-20">
    <div class="sin-margen-izquierdo sin-margen-derecho linea-separadora-superior-completa-negra subtitulo-grupo-noticias-container">
      <div class="subtitulo-grupo-noticias">
        <img src="{!! App\asset_path('images/headlines_back.jpg') !!}" alt="headlines_back" title="headlines_back" />
        <div class="texto-centrado">
          <h2>Minería</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-7 col-lg-5 col-xl-6 linea-separadora-derecha-completa-gris">
        @if (FrontPage::havePost('mineria'))
        <article class="noticia-margen-sidebar-izquierdo noticia noticia-destacada noticia-vertical-completa con-margen-inferior">
          <div class="noticia-imagen con-margen-inferior">
            <a href="{!! FrontPage::postLink() !!}">
              <figure>
                {!! FrontPage::postImagenHome('547×223') !!}
              </figure>
            </a>
          </div>
          <header>
            <div class="noticia-volanta">{!! FrontPage::postVolanta() !!}</div>
            <div class="noticia-titulo">
              <a href="{!! FrontPage::postLink() !!}">
                <h3>{!! FrontPage::postTitle() !!}</h3>
              </a>
            </div>
          </header>
          <div class="noticia-texto">{!! FrontPage::postContenido() !!}</div>
        </article>
        @php
        FrontPage::resetPost()
        @endphp
        @endif
        @if (FrontPage::havePost('mineria'))
        <article class="noticia-margen-sidebar-izquierdo noticia noticia-destacada">
          <div class="separator"></div>
          <header>
            <div class="noticia-titulo">
              <a href="{!! FrontPage::postLink() !!}">
                <h3>{!! FrontPage::postTitle() !!}</h3>
              </a>
            </div>
          </header>
          <div class="noticia-texto">{!! FrontPage::postContenido() !!}</div>
        </article>
        @php
        FrontPage::resetPost()
        @endphp
        @endif
      </div>
      <div class="col-12 col-md-5 col-lg-3 col-xl-2">
        @if (FrontPage::havePost('mineria'))
        <article class="noticia noticia-vertical">
          <div class="separator"></div>
          <div class="d-none d-md-block noticia-imagen">
            <a href="{!! FrontPage::postLink() !!}">
              <figure>
                {!! FrontPage::postImagenHome('547×255') !!}
              </figure>
            </a>
          </div>
          <div class="noticia-titulo">
            <a href="{!! FrontPage::postLink() !!}">
              <h3>{!! FrontPage::postTitle() !!}</h3>
            </a>
          </div>
          <div class="noticia-texto" style="margin-bottom: 10px; margin-top: -11px">{!! FrontPage::postContenido() !!}</div>
        </article>
        @php
        FrontPage::resetPost()
        @endphp
        @endif
        @if (FrontPage::havePost('mineria'))
        <article class="noticia noticia-vertical">
          <div class="separator"></div>
          <div class="d-none d-md-block noticia-imagen">
            <a href="{!! FrontPage::postLink() !!}">
              <figure>
                {!! FrontPage::postImagenHome('547×255') !!}
              </figure>
            </a>
          </div>
          <div class="noticia-titulo">
            <a href="{!! FrontPage::postLink() !!}">
              <h3>{!! FrontPage::postTitle() !!}</h3>
            </a>
          </div>
        </article>
        @php
        FrontPage::resetPost()
        @endphp
        @endif
      </div>
      <div class="col-12 col-lg-4">
        @if (App\has_banners('home', array('banner-14', 'banner-15', 'banner-16', 'banner-17')))
        <div class="banner-vertical banner sin-margen-derecho">
          @if (App\has_banner('home', 'banner-14'))
            <div class="banner-vertical-1 centar-contenido">
              {!! App\get_banners('home', 'banner-14') !!}
            </div>
          @endif
          @if (App\has_banner('home', 'banner-15'))
            <div class="banner-vertical-2 centar-contenido">
              {!! App\get_banners('home', 'banner-15') !!}
            </div>
          @endif
          @if (App\has_banner('home', 'banner-16'))
            <div class="banner-vertical-3 centar-contenido">
              {!! App\get_banners('home', 'banner-16') !!}
            </div>
          @endif
          @if (App\has_banner('home', 'banner-17'))
            <div class="banner-vertical-4 centar-contenido">
              {!! App\get_banners('home', 'banner-17') !!}
            </div>
          @endif
        </div>
        @endif
      </div>
    </div>
  </section>