@if (have_rows("slider_video", 'options'))
  <section id="grupoVideosDestacados" class="margen-superior-20">
    <div class="sin-margen-izquierdo sin-margen-derecho linea-separadora-superior-completa-negra subtitulo-grupo-noticias-container">
      <div class="subtitulo-grupo-noticias">
        <img src="{!! App\asset_path('images/headlines_back.jpg') !!}" alt="headlines_back" title="headlines_back" />
        <div class="texto-centrado">
          <h2>Videos destacados</h2>
        </div>
      </div>
    </div>
    <div class="row noticias-seccion-especial-container">
      <div class="col-2 col-md-1 align-self-center centar-contenido sin-padding-izquierdo sin-padding-derecho">
        <a href="#carousel-home-video" role="button" data-slide="prev">
          <img src="{!! App\asset_path('images/flecha_izq.jpg') !!}" title="flecha izquierda" alt="flecha izquerda" />
        </a>
      </div>
      <div class="col-8 col-md-10">
        <div id="carousel-home-video" class="carousel slide" data-ride="carousel" data-interval="0">
          <div class="carousel-inner">
            @php
              $c=0;
            @endphp
            @while ($c < 3)
            <div class="carousel-item @if ($i == 0) active @endif">
              <div class="row">
                @for ($i = 0; $i < 5; $i++)
                @php
                  $hay = have_rows("slider_video", 'options');
                  if (!$hay) {
                    echo '</div></div>';
                    break 2;
                  }
                  the_row();
                  $code = get_sub_field('video');
                @endphp
              <div class="col-12 col-sm linea-separadora-derecha-completa-gris">
                <article class="noticia con-margen-inferior">
                  <div class="noticia-imagen">
                    <a href="https://www.youtube.com/watch?v=<?php echo $code ?>" data-toggle="modal" data-target="#video<?php echo md5($code); ?>">
                      <figure>
                        <img src="https://img.youtube.com/vi/<?php echo $code ?>/0.jpg" loading="lazy" />
                      </figure>
                    </a>
                  </div>
                  <div class="noticia-titulo">
                    <a href="https://www.youtube.com/watch?v=<?php echo $code ?>" data-toggle="modal" data-target="#video<?php echo md5($code); ?>">
                      <h3><?php the_sub_field('titulo'); ?></h3>
                    </a>
                  </div>
                </article>
              </div>
            @endfor

              </div>
            </div>
            @php
              $c++;
            @endphp
            @endwhile


          </div>
        </div>
      </div>
      <div class="col-2 col-md-1 align-self-center centar-contenido sin-padding-izquierdo sin-padding-derecho">
        <a href="#carousel-home-video" role="button" data-slide="next">
          <img src="{!! App\asset_path('images/flecha_der.jpg') !!}" title="flecha derecha" alt="flecha derecha" />
        </a>
      </div>
    </div>
  </section>
@php
  $c=0;
@endphp
@while (have_rows("slider_video", 'options'))
  @php
  $c++;
  if ($c > 15) break;
  the_row(); 
  $code = get_sub_field('video')
  @endphp
  <div id="video<?php echo md5($code); ?>" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><?php the_sub_field('titulo'); ?></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body"><div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="//www.youtube.com/embed/<?php echo $code; ?>"></iframe></div></div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <script>;(function($) { $('#video<?php echo md5($code); ?>').on('hidden.bs.modal', function (e) { jQuery("#video<?php echo md5($code); ?> iframe").attr("src", jQuery("#video<?php echo md5($code); ?> iframe").attr("src")); })})(jQuery);</script>
@endwhile

@endif