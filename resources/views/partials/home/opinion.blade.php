<section id="grupoOpinion" class="margen-superior-20">
  <div class="sin-margen-izquierdo sin-margen-derecho linea-separadora-superior-completa-negra subtitulo-grupo-noticias-container">
    <div class="subtitulo-grupo-noticias">
      <img src="{!! App\asset_path('images/headlines_back.jpg') !!}" alt="headlines_back" title="headlines_back" />
      <div class="texto-centrado">
        <h2>Opinión</h2>
      </div>
    </div>
  </div>
  <div class="row noticias-seccion-especial-container">
    <div class="col-2 col-md-1 align-self-center centar-contenido sin-padding-izquierdo sin-padding-derecho">
      <a href="#carousel-home-opinion" role="button" data-slide="prev">
        <img src="{!! App\asset_path('images/flecha_izq.jpg') !!}" title="flecha izquierda" alt="flecha izquerda" />
      </a>
    </div>
    <div class="col-8 col-md-10">
      <div id="carousel-home-opinion" class="carousel slide" data-ride="carousel" data-interval="0">
        <div class="carousel-inner">

          @for ($i = 0; $i < 2; $i++)
          <div class="carousel-item @if ($i == 0) active @endif">
            <div class="row">
              @for ($x = 0; $x < 5; $x++)
              @if (FrontPage::havePost('opinion'))
              <div class="col-12 col-sm linea-separadora-derecha-completa-gris">
                <article class="noticia con-margen-inferior">
                  <div class="noticia-imagen">
                    <a href="{!! FrontPage::postLink() !!}">
                      <figure>
                        {!! FrontPage::postImagenHome('540×362') !!}
                      </figure>
                    </a>
                  </div>
                  <div class="noticia-volanta">
                    {!! Category::postVolanta() !!}
                  </div>
                  <div class="noticia-titulo">
                    <a href="{!! FrontPage::postLink() !!}">
                      <h3>{!! FrontPage::postTitle() !!}</h3>
                    </a>
                  </div>
                </article>
              </div>
              @php
              FrontPage::resetPost()
              @endphp
              @endif
              @endfor
            </div>
          </div>
          @endfor
        </div>
      </div>
    </div>
    <div class="col-2 col-md-1 align-self-center centar-contenido sin-padding-izquierdo sin-padding-derecho">
      <a href="#carousel-home-opinion" role="button" data-slide="next">
        <img src="{!! App\asset_path('images/flecha_der.jpg') !!}" title="flecha derecha" alt="flecha derecha" />
      </a>
    </div>
  </div>
</section>