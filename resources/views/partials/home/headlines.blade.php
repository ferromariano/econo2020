<section id="grupoHeadlines">
  <div class="row">
    <div class="col-12 col-xl-10">
      <div class="sin-margen-izquierdo">
        @if (App\has_banner('home', 'wide-1'))
          <div class="banner-horizontal banner linea-separadora-superior-completa-negra">
            <div class="centar-contenido">
              {!! App\get_banners('home', 'wide-1') !!}
            </div>
          </div>
        @endif
        @if (App\has_banner('home', 'banner-0'))
          <div class="row justify-content-center d-flex d-lg-none con-margen-inferior">
            <div class="banner-vertical-rectangular banner">
              {!! App\get_banners('home', 'banner-0') !!}
            </div>
          </div>
        @endif
        <!--================SUBGRUPO=============-->
        <div class="linea-separadora-superior-completa-negra subtitulo-grupo-noticias-container">
          <div class="subtitulo-grupo-noticias">
            <img src="{!! App\asset_path('images/headlines_back.jpg') !!}" alt="headlines_back" title="headlines_back" />
            <div class="texto-centrado">
              <h2>Headlines</h2>
            </div>
          </div>
        </div>
        <!--================SUBGRUPO=============-->
      </div>
      <div class="row">
        <div class="col-12 col-lg-8">
          <div class="row">
            <div class="col-12 col-md-7 col-lg-8 linea-separadora-derecha-completa-gris">
              <div class="row">
                <div class="col-auto d-none d-lg-flex">
                  <div class="left-sidebar-container fecha-container">
                    <div class="fecha-dia">
                      {!! wp_date('D d') !!}
                    </div>
                    <div class="fecha-mes">
                      {!! wp_date('F Y') !!}
                    </div>
{{--                     <div class="temperatura">
                      12º
                    </div>
                    <div class="ubicacion">
                      Buenos Aires
                    </div>
 --}}                  
                  </div>
                </div>
                <div class="col">
                  @if (get_field( "is_video_en_vivo", 'options' ))
                    <article class="noticia noticia-destacada noticia-margen-container">
                      <div class="noticia-imagen">
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ get_field( "video_en_vivo", 'option' ) }}?rel=0;&autoplay=1" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen include></iframe>
                        </div>
                      </div>
                      <header>
                        <div class="noticia-titulo">
                          <h3>{{ get_field( "titulo_en_vivo", 'option' ) }}</h3>
                        </div>
                      </header>
                      <div class="noticia-texto">
                        {{ get_field( "descripcion_en_vivo", 'option' ) }}
                      </div>
                    </article>
                  @else
                    @if (FrontPage::havePost('destacada'))
                    <article class="noticia noticia-destacada noticia-margen-container">
                      <div class="noticia-imagen">
                        <a href="{!! FrontPage::postLink() !!}">
                          <figure>
                            {!! FrontPage::postImagenHome('537×298') !!}
                          </figure>
                        </a>
                      </div>
                      <header>
                        <div class="noticia-titulo">
                          <a href="{!! FrontPage::postLink() !!}">
                            <h3>{!! FrontPage::postTitle() !!}</h3>
                          </a>
                        </div>
                      </header>
                      <div class="noticia-texto">
                        {!! FrontPage::postContenido() !!}
                      </div>
                    </article>
                    @php
                    FrontPage::resetPost()
                    @endphp
                    @endif

                  @endif
                </div>
              </div>
            </div>
            <div class="col-12 col-md-5 col-lg-4">
              @if (FrontPage::havePost('destacada'))
              <article class="noticia noticia-vertical">
                <div class="d-none d-md-block noticia-imagen">
                  <a href="#">
                    <figure>
                      {!! FrontPage::postImagenHome('340×168') !!}
                    </figure>
                  </a>
                </div>
                <header>
                  <div class="noticia-titulo">
                    <a href="{!! FrontPage::postLink() !!}">
                      <h3>{!! FrontPage::postTitle() !!}</h3>
                    </a>
                  </div>
                </header>
                <div class="separator"></div>
              </article>
              @php
              FrontPage::resetPost()
              @endphp
              @endif

              @if (FrontPage::havePost('destacada'))
              <article class="noticia noticia-vertical">
                <div class="d-none d-md-block noticia-imagen">
                  <a href="{!! FrontPage::postLink() !!}">
                    <figure>
                      {!! FrontPage::postImagenHome('340×168') !!}
                    </figure>
                  </a>
                </div>
                <header>
                  <div class="noticia-titulo">
                    <a href="{!! FrontPage::postLink() !!}">
                      <h3>{!! FrontPage::postTitle() !!}</h3>
                    </a>
                  </div>
                </header>
              </article>
              @php
              FrontPage::resetPost()
              @endphp
              @endif

            </div>
            <div class="col-12">
              @if (FrontPage::havePost('destacada'))
              <article class="noticia-margen-sidebar-izquierdo noticia noticia-horizontal linea-separadora-superior-completa-gris">
                <header>
                  <div class="noticia-volanta">
                    {!! FrontPage::postVolanta() !!}
                  </div>
                  <div class="noticia-titulo">
                    <a href="{!! FrontPage::postLink() !!}">
                      <h3>{!! FrontPage::postTitle() !!}</h3>
                    </a>
                  </div>
                  <div class="noticia-autor">
                    <a href="#">
                      {!! FrontPage::postAutor() !!}
                    </a>
                  </div>
                </header>
                <div class="noticia-texto">
                  {!! FrontPage::postContenido() !!}
                </div>
              </article>
              @php
              FrontPage::resetPost()
              @endphp
              @endif

            </div>
          </div>
        </div>
        <div class="col-12 col-lg-4">
          @if (App\has_banners( 'home', array('banner-1','banner-2','banner-3','banner-4') ))
          <div class="banner-vertical banner">
            @if ( App\has_banner('home', 'banner-1') )
            <div class="banner-vertical-1 centar-contenido d-none d-lg-flex">
              {!! App\get_banners('home', 'banner-1') !!}
            </div>
            @endif
            @if ( App\has_banner('home', 'banner-2') )
            <div class="banner-vertical-2 centar-contenido">
              {!! App\get_banners('home', 'banner-2') !!}
            </div>
            @endif
            @if ( App\has_banner('home', 'banner-3') )
            <div class="banner-vertical-3 centar-contenido">
              {!! App\get_banners('home', 'banner-3') !!}
            </div>
            @endif
            @if ( App\has_banner('home', 'banner-4') )
            <div class="banner-vertical-4 centar-contenido">
              {!! App\get_banners('home', 'banner-4') !!}
            </div>
            @endif
          </div>
          @endif
        </div>
      </div>
    </div>
    <div class="d-none col-xl-2 d-xl-block">
      <aside class="right-sidebar sin-margen-derecho">
        @include('partials.assets.mas-leidas')
      </aside>
    </div>
  </div>
</section>