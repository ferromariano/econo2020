@if (App\has_banners('search', array('banner-1', 'banner-2', 'banner-3', 'banner-4', 'banner-5', 'banner-6')))
<div class="banner-vertical banner sin-margen-derecho">
  <div class="banner-vertical banner sin-margen-derecho">
    @if ( App\has_banner('search', 'banner-1') )
    <div class="banner-vertical-1 banner-vertical-rectangular centar-contenido d-none d-lg-flex">
      {!! App\get_banners('search', 'banner-1') !!}
    </div>
    @endif
    @if ( App\has_banner('search', 'banner-2') )
    <div class="banner-vertical-2 banner-vertical-cuadrado centar-contenido">
      {!! App\get_banners('search', 'banner-2') !!}
    </div>
    @endif
    @if ( App\has_banner('search', 'banner-3') )
    <div class="banner-vertical-3 banner-vertical-rectangular centar-contenido">
      {!! App\get_banners('search', 'banner-3') !!}
    </div>
    @endif
    @if ( App\has_banner('search', 'banner-4') )
    <div class="banner-vertical-4 banner-vertical-rectangular centar-contenido">
      {!! App\get_banners('search', 'banner-4') !!}
    </div>
    @endif
    @if ( App\has_banner('search', 'banner-5') )
    <div class="banner-vertical-5 banner-vertical-rectangular centar-contenido">
      {!! App\get_banners('search', 'banner-5') !!}
    </div>
    @endif
    @if ( App\has_banner('search', 'banner-6') )
    <div class="banner-vertical-6 banner-vertical-rectangular centar-contenido">
      {!! App\get_banners('search', 'banner-6') !!}
    </div>
    @endif
  </div>
</div>
@endif