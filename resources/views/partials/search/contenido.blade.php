@php
  global $wp_query;

  $txt_tag='';
  $istag=false;
  if (is_tag()): 
    $term    = get_queried_object(); 
    $txt_tag = $term->name;
    $istag=is_tag();
  else:
    if (is_404()):
      $txt_tag = get_query_var('tag', false);
      $istag=true;
    endif;
  endif;

function has_query_arg($k, $v) {
  if (!isset($_GET[$k])) { return false; }
  if ($_GET[$k] != $v ) { return false; }
  return true;
}


@endphp
<div class="row">
  <div class="col-12">
    @if (App\has_banner('search', 'wide-1'))
    <div class="row justify-content-center linea-separadora-superior-completa-negra d-none d-lg-flex">
      <div class="banner-horizontal banner">
        {!! App\get_banners('search', 'wide-1') !!}
      </div>
    </div>
    @endif
    @if (App\has_banner('search', 'banner-0'))
    <div class="row justify-content-center d-flex d-lg-none con-margen-inferior">
      <div class="banner-vertical-rectangular banner">
        {!! App\get_banners('search', 'banner-0') !!}
      </div>
    </div>
    @endif
  </div>
</div>
<!--==================CONTENIDO UNICO===============================-->
<section id="busqueda">
  <div class="row linea-separadora-superior-completa-negra">
    <div class="col-12 col-md-7 col-lg-8 con-margen-superior">
      <div class="row">
        <div class="col-lg-3 d-none d-lg-flex">
{{--           <div class="left-sidebar-container fecha-container">
            <div class="fecha-dia">
              miérc. 20
            </div>
            <div class="fecha-mes">
              julio 2019
            </div>
            <div class="temperatura">
              12º
            </div>
            <div class="ubicacion">
              Buenos Aires
            </div>
          </div>
          --}}
        </div>
        <div class="col">
          <div class="search-container">
            <form action="{!! esc_url(home_url('/')) !!}">
              <div class="search">
                <input style="width: 100%;" id="tag-input-tag" name="tag" type="search" class="search-header @php if (!$istag)      echo 'd-none'; @endphp" placeholder="BUSCAR EN ECONOJOURNAL" value="{!! $txt_tag !!}">
                <input style="width: 100%;" id="tag-input-s"   name="s"   type="search" class="search-header @php if (!is_search()) echo 'd-none'; @endphp" placeholder="BUSCAR EN ECONOJOURNAL" value="{!! get_search_query() !!}">
                <i class="fas fa-search"></i>
              </div>
              <span class="labelOption">BUSCAR POR ETIQUETA </span><input id="tipo-tag" type="radio" name="tipo" value="tag" @php if ($istag)      echo 'checked'; @endphp>
              <span class="labelOption">BUSCAR POR CONTENIDO</span><input id="tipo-s"   type="radio" name="tipo" value="s"   @php if (is_search()) echo 'checked'; @endphp>
            </form>
          </div>
          <h2>Se encontraron {!! $wp_query->found_posts !!} resultados</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-lg-3 d-lg-flex">
          <div id="mostrarFiltros" class="d-block d-lg-none con-margen-superior">
            <i class="fas fa-chevron-up"></i><i class="fas fa-chevron-down"></i>FILTRAR<i class="fas fa-chevron-down"></i><i class="fas fa-chevron-up"></i>
          </div>
          <div id="filtros">
            <div class="row justify-content-center">
              <div class="col-5 offset-1 col-md-3 offset-md-1 col-lg-12">
                <div class="filters con-margen-superior">
                  <label>Ordenar por</label>
                  <a class="@php echo (has_query_arg('order', 'DESC'))   ? 'activate_b' : ''; @endphp" href="@php echo (has_query_arg('order', 'DESC'))   ? remove_query_arg( 'order' )      : add_query_arg( array('order'=>'DESC') ) @endphp" rel="order">MÁS nuevo</a>
                  <a class="@php echo (has_query_arg('order', 'ASC'))    ? 'activate_b' : ''; @endphp" href="@php echo (has_query_arg('order', 'ASC'))    ? remove_query_arg( 'order' )      : add_query_arg( array('order'=>'ASC') ) @endphp" rel="order">MÁS ANTIGUO</a>
                </div>
              </div>
              <div class="col-5 offset-1 col-md-3 offset-md-1 col-lg-12">
                <div class="filters con-margen-superior">
                  <label>Filtrar por</label>
                  <a class="@php echo (has_query_arg('date_limit', '1')) ? 'activate' : ''; @endphp" href="@php echo (has_query_arg('date_limit', '1')) ? remove_query_arg( 'date_limit' ) : add_query_arg( array('date_limit'=>'1') ); @endphp" rel="filterfor">ÚLTIMO DÍA</a>
                  <a class="@php echo (has_query_arg('date_limit', '2')) ? 'activate' : ''; @endphp" href="@php echo (has_query_arg('date_limit', '2')) ? remove_query_arg( 'date_limit' ) : add_query_arg( array('date_limit'=>'2') ); @endphp" rel="filterfor">ÚLTIMO mes</a>
                  <a class="@php echo (has_query_arg('date_limit', '3')) ? 'activate' : ''; @endphp" href="@php echo (has_query_arg('date_limit', '3')) ? remove_query_arg( 'date_limit' ) : add_query_arg( array('date_limit'=>'3') ); @endphp" rel="filterfor">ÚLTIMO año</a>
                </div>
              </div>
              <div class="col-12">
                <div class="filters con-margen-superior">
                  <div class="post-meta">
                    @foreach (get_categories('exclude=1') as $element)
                    <a class="@php echo (has_query_arg('cat', $element->cat_ID)) ? 'activate' : ''; @endphp" href="@php echo (has_query_arg('cat', $element->cat_ID)) ? remove_query_arg( 'cat' ) : add_query_arg( array('cat'=>$element->cat_ID) ); @endphp" rel="category tag">#{!! $element->name !!}</a>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col">
          <!--==================NOTA RELACIONADAS===============================-->
          <section id="notas-listado">
            <div class="row">
              <div class="col-12">

                @while (have_posts()) 
                @php the_post() @endphp
                <article class="noticia">
                  <div class="noticia-contenido">
                    <div class="separator"></div>
                    <header>
                      <div class="noticia-volanta">
                        @foreach (Search::postTaxos('category') as $element)
                        <a class="volanta-seccion" href="{{ $element['l'] }}">#{!! $element['n'] !!}</a>
                        @endforeach  

                        {{ Search::postDate() }}
                      </div>
                      <div class="noticia-titulo">
                        <a href="{{ Search::postLink() }}">
                          <h2>{{ Search::postTitle() }}</h2>
                        </a>
                      </div>
                    </header>
                    <div class="noticia-texto">
                      {{ Search::postCopete() }}
                    </div>
                  </div>
                </article>
                @endwhile
              </div>
            </div>
          </section>
          <div class="search-pagination-container">
            <div class="row justify-content-center">
              <div class="col">
                {!! App\pagenavi($before = '<ul>', $after = '</ul>') !!}
              </div>
            </div>
          </div>
          <!--==================NOTA RELACIONADAS===============================-->
        </div>
      </div>
    </div>
    <div class="col-12 col-md-5 col-lg-4 con-margen-superior">
      <div class="sidebar-right-container">
        @include('partials.search.banner1')
      </div>
    </div>
  </div>
  @if (App\has_banner('search', 'wide-2'))
  <div class="row justify-content-center linea-separadora-superior-completa-negra linea-separadora-inferior-completa-negra con-margen-superior d-none d-lg-flex">
    <div class="banner-horizontal banner">
      {!! App\get_banners('search', 'wide-2') !!}
    </div>
  </div>
  @endif
</section>
<!--==================CONTENIDO UNICO===============================-->
<!--==================FORMULARIO ANUNCIANTES============================-->
@include('partials.assets.form-anunciantes')

@section('after_page')
<script>
(function($){
  $(document).ready(function() {
    $('#tipo-tag').change(function() {
      $('#tag-input-tag').removeClass('d-none');
      $('#tag-input-s').addClass('d-none');
    });
    $('#tipo-s').change(function() {
      $('#tag-input-s').removeClass('d-none');
      $('#tag-input-tag').addClass('d-none');
    });


    var contentInputSearch;
    $("#tag-input-tag").bind('keyup', function() {
      var $s = $(this).val();
      if( $s.length < 2 ) { 
        $("#tag-input-tag").removeClass('loading'); 
        return; 
      }
      if( $("#tag-input-tag").hasClass('loading') ) { return; }
      $("#tag-input-tag").addClass('loading');

      if( $('.tag-input-tag-list').length === 0 ) { 
        contentInputSearch = $("<div class='tag-input-tag-list'><div/>").insertAfter("#tag-input-tag"); 
      } else { 
        contentInputSearch = $('.tag-input-tag-list'); 
      }
      $( contentInputSearch ).html('');
      $.ajax({
        url: '{!! admin_url('admin-ajax.php') !!}',
        dataType: 'json',
        data: { action: 'search_tag', tag_s: $s, _ajax_nonce: '{!! wp_create_nonce('search_tag') !!}' },
        success: function($d) {
          
          $("#tag-input-tag").removeClass('loading');
          var ul = $('<ul></ul>').appendTo(contentInputSearch);
          $.each( $d, function(i, d) {
            $li = $('<li></li>').appendTo( ul );
            $link = $('<a></a>').html( d.text )
                                .attr('href', d.link ).appendTo( $li );
          });
        }
      });
    }).focusout(function() {
      // window.setTimeout(function() {
      //   $('.tag-input-tag-list').remove();
      // }, 800);
    });
  });
})(jQuery);
</script>
@endsection
