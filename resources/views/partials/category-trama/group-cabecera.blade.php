<section id="grupoRevistaTrama" class="margen-superior-20">
  <div class="sin-margen-izquierdo sin-margen-derecho linea-separadora-superior-completa-negra subtitulo-grupo-noticias-container">
    <div class="subtitulo-grupo-noticias">
      <h2>REVISTA TRAMA / {!! FrontPage::TramaUltimaEdicion()->name !!}</h2>
    </div>
  </div>
  <div id="revista-trama-container" class="row">
    <div class="col-12">
      @if (Category::havePost('category'))
      <article class="noticia con-margen-inferior">
        <div class="row">
          <div class="col-12 col-md-6 col-xl-5">
            <div class="noticia-imagen">
              <a href="{!! Category::postLink() !!}">
                <figure>
                  {!! Category::postImagenHome('544×406') !!}
                </figure>
              </a>
            </div>
          </div>
          <div class="col-12 col-md-6 col-xl-5">
            <header>
              <div class="noticia-volanta con-margen-inferior">
                {!! Category::postVolanta() !!}
              </div>
              <div class="noticia-titulo con-margen-superior con-margen-inferior">
                <a href="{!! Category::postLink() !!}">
                  <h3>{!! Category::postTitle() !!}</h3>
                </a>
              </div>
            </header>
            <div class="noticia-texto con-margen-inferior">{!! Category::postContenido() !!}</div>
            <div class="noticia-autor">
              <a href="{!! Category::postLink() !!}">
                {!! Category::postAutor() !!}
              </a>
            </div>
          </div>
          <div class="d-none d-xl-flex col-xl-2">
            <div class="d-none d-md-block noticia-imagen">
              <a href="{!! Category::postLink() !!}">
                <figure>
                  {!! Category::postImage2('200×406') !!}
                </figure>
              </a>
            </div>
          </div>
        </div>
      </article>
      @php
      Category::resetPost()
      @endphp
      @endif
    </div>
    <div class="w-100"></div>
    <div class="col-12 con-margen-superior">
      <div id="revista-trama-inferior-container">
        <div class="row">
          @for ($i = 0; $i < 3; $i++)
            @if (Category::havePost('category'))
              <div class="col-12 col-lg-4 linea-separadora-superior-completa-negra">
                <article class="noticia">
                  @if (Category::hasImagenHome())
                    <div class="noticia-imagen"><a href="{!! Category::postLink() !!}"><figure>{!! Category::postImagenHome('120×124') !!}</figure></a></div>
                  @endif
                  <div class="noticia-contenido">
                    <div class="separator"></div>
                    <header>
                      <div class="noticia-volanta">{!! Category::postVolantaCompuesta() !!} {!! Category::postVolanta() !!}</div>
                      <div class="noticia-titulo"><a href="{!! Category::postLink() !!}"><h3>{!! Category::postTitle() !!}</h3></a></div>
                    </header>
                  </div>
                </article>
              </div>
              @php Category::resetPost() @endphp
            @endif
          @endfor
        </div>
      </div>
    </div>
  </div>
  @if (App\has_banner('category', 'wide-2'))
  <div class="row justify-content-center linea-separadora-superior-completa-negra linea-separadora-inferior-completa-negra d-none d-lg-flex">
    <div class="banner-horizontal banner">
      {!! App\get_banners('category', 'wide-2') !!}
    </div>
  </div>
  @endif
</section>
