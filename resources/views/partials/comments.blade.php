@if (comments_open())
  @php
    if (post_password_required()) { return; }
  @endphp


  @if (have_comments())
  <div id="comments" class="comments">
    <div class="comments-area">
      <h2 class="comments-title">
  {!! 
    sprintf(
      _nx(
        'Una respuesta a %2$s', 
        '%1$s respuestas a %2$s', 
        get_comments_number(), 
        'comments title', 
        'sage'
      ), 
      number_format_i18n(
        get_comments_number()
      ), 

      '<span>“' . get_the_title() . '”</span>') 

  !!}</h2>
      <ol class="comments-list list-unstyled" id="comments-list">
        {!! wp_list_comments(['style' => 'ol', 'short_ping' => true]) !!}
      </ol>

      @if (get_comment_pages_count() > 1 && get_option('page_comments'))
        <div class="comments-nav-section cf">
          @if (get_previous_comments_link())
          <p class="alignleft">@php previous_comments_link(__('&larr; Comentarios Anteriores', 'sage')) @endphp</p>
          @endif
          @if (get_next_comments_link())
          <p class="alignright">@php next_comments_link(__('Comentarios Recientes &rarr;', 'sage')) @endphp</p>
          @endif
        </div>
      @endif
    </div>
  </div>
  @endif


  <!--==================FORMULARIO COMENTARIO============================-->
  <section id="formulario-comentario">
    <div class="form-container">
      <div class="wpcf7">
        @php 
        
        $commenter     = wp_get_current_commenter();

        comment_form(array(
          'comment_field' => '<div class="row">'
                            .'  <div class="col-12 col-lg-6 order-0 order-lg-1">',
          'submit_field'  => '  </div>'
                            .'    <div class="col-12 col-lg-6 order-1 order-lg-0">'
                            .'    <p class="comment-form-comment"><label for="comment">Comentario</label> <textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" required="required"></textarea></p>'
                            .'    <p class="form-submit">%1$s %2$s</p>'
                            .'    <p><input type="checkbox" checked /> <span class="labelCheck">Quiero recibir Newsletter</span></p>'
                            .'  </div>'
                            .'</div>',
          'class_form'    => 'wpcf7-form',
          'fields'        => array(
            'author'        => sprintf( '<p><label>Nombre*<br><span class="wpcf7-form-control-wrap your-name"><input type="text" name="author" value="%s" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span></label></p>',  esc_attr( $commenter['comment_author'] )),
            'email'         => sprintf( '<p><label>Email* <br><span class="wpcf7-form-control-wrap email"    ><input type="text" name="email"  value="%s" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span></label></p>',  esc_attr( $commenter['comment_author_email'] )),
            'url'           => sprintf( '<p><label>Web    <br><span class="wpcf7-form-control-wrap web"      ><input type="text" name="url"    value="%s" size="40" class="wpcf7-form-control wpcf7-text" aria-required="true" aria-invalid="false"></span></label></p>',                              esc_attr( $commenter['comment_author_url'] )),
          )


          )) @endphp
      </div>
    </div>
  </section>
@endif
