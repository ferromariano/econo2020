@include('partials.single-post.banners1')
<!--==================NOTA===============================-->
<section id="contenido-unico">
  <div class="row linea-separadora-superior-completa-negra">
    <div class="col-12 col-md-7 col-lg-8">
      <div class="sin-margen-izquierdo sin-margen-derecho subtitulo-grupo-noticias-container">
        <div class="subtitulo-grupo-noticias">
          <img src="{!! App\asset_path('images/headlines_back.jpg') !!}" alt="headlines_back" title="headlines_back" />
          <div class="texto-centrado">
            <h2>
              @foreach (get_the_category() as $element)
                {{ $element->name }}
              @endforeach
            </h2>
          </div>
        </div>
      </div>
      <article @php 
        post_class("noticia") 
        @endphp>
        <header>
{{--           <div class="noticia-fecha"><time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time></div> --}}
          <div class="noticia-volanta">{!! FrontPage::postVolanta() !!}</div>
          <div class="noticia-titulo"><h1>{!! get_the_title() !!}</h1></div>
          <div class="noticia-autor">{!! FrontPage::postAutor() !!}</div>
        </header>

        @include('partials.single-post.social')

        <div class="row linea-separadora-superior-completa-negra con-margen-inferior con-margen-superior no-gutters sin-margen-izquierdo"></div>
        <div class="row">
          <div class="col-auto d-none d-lg-flex">
            <div class="left-sidebar-container fecha-container">
              <div class="fecha-dia">{{ get_the_date('j l') }}</div>
              <div class="fecha-mes">{{ get_the_date('F Y') }}</div>
{{--
              <div class="temperatura">12º</div>
              <div class="ubicacion">Buenos Aires</div>
--}}
            </div>
          </div>
          <div class="col">
            <div class="noticia-texto">
              <p class="copete">{!! FrontPage::postContenido() !!}</p>

              @include('partials.single-post.banners2')

              @php 
                the_content() 
              @endphp

            </div>

            @include('partials.single-post.banners3')

            <div class="post-meta">
              {!! get_the_tag_list( '<p class="post-meta-tag">Etiquetas:</p>', ' ' ) !!}
            </div>
            <div class="posts-navigation-container linea-separadora-superior-completa-negra">
              <div class="row justify-content-between">
                <div class="col col-lg-4">{!! get_previous_post_link( '%link', '<div><span><i class="fas fa-chevron-left"></i>anterior</span></div><div class="con-margen-superior">%title</div>' ) !!}</div>
                <div class="col col-lg-4">{!! get_next_post_link(     '%link', '<div class="alineacion-derecha"><span>siguiente<i class="fas fa-chevron-right"></i></span></div><div class="alineacion-derecha con-margen-superior">%title</div>') !!}</div>
              </div>
            </div>

            @include('partials.single-post.comentarios')

          </div>
        </div>
      </article>
    </div>
    <div class="d-none d-md-flex col-md-5 col-lg-4">
      <div class="sidebar-right-container">

        @include('partials.single-post.banners4')

        <aside class="d-none d-md-flex right-sidebar sin-margen-derecho">
          @include('partials.assets.mas-leidas')
        </aside>
      </div>
    </div>
  </div>
  @include('partials.single-post.banners5')
</section>
<!--==================NOTA===============================-->
<!--==================NOTA RELACIONADAS===============================-->
@include('partials.single-post.relacionadas')
<!--==================NOTA RELACIONADAS===============================-->
<!--==================FORMULARIO ANUNCIANTES============================-->
@include('partials.assets.form-anunciantes')
