<div class="row">
  <div class="col-12">
    @if (App\has_banners('single', array('wide-1','banner-0')))
      @if (App\has_banner('single', 'wide-1'))
      <div class="row justify-content-center linea-separadora-superior-completa-negra d-none d-lg-flex">
        <div class="banner-horizontal banner">
          {!! App\get_banners('single', 'wide-1') !!}
        </div>
      </div>
      @endif
      @if (App\has_banner('single', 'banner-0'))
      <div class="row justify-content-center d-flex d-lg-none con-margen-inferior">
        <div class="banner-vertical-rectangular banner">
          {!! App\get_banners('single', 'banner-0') !!}
        </div>
      </div>
      @endif
    @endif
  </div>
</div>
