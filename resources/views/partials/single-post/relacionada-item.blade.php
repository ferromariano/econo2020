<article class="noticia">
  <div class="row">
    <div class="d-none d-md-flex col-md-3">
      @if (SinglePost::hasImagenHome())
      <div class="noticia-imagen">
        <a href="{!! SinglePost::postLink() !!}">
          <figure>
            {!! SinglePost::postImagenHome('170×135') !!}
          </figure>
        </a>
      </div>
      @endif
    </div>
    <div class="col-12 col-md-9">
      <div class="noticia-contenido">
        <div class="separator"></div>
        <header>
          <div class="noticia-volanta">{!! SinglePost::postVolanta() !!}</div>
          <div class="noticia-titulo">
            <a href="{!! SinglePost::postLink() !!}">
              <h2>{!! SinglePost::postTitle() !!}</h2>
            </a>
          </div>
        </header>
        <div class="noticia-texto">{!! SinglePost::postContenido() !!}</div>
      </div>
    </div>
  </div>
</article>