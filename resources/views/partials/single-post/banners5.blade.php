@if ( App\has_banner('single', 'wide-2') )
<div class="row justify-content-center linea-separadora-superior-completa-negra linea-separadora-inferior-completa-negra con-margen-superior d-none d-lg-flex">
  <div class="banner-horizontal banner">
  	{!! App\get_banners('single', 'wide-2') !!}
  </div>
</div>
@endif
