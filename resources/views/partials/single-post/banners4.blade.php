@if (App\has_banners('single', array('banner-6','banner-7','banner-8','banner-9','banner-10','banner-11','banner-12','banner-13','banner-14','banner-15','banner-16')))
  <div class="banner-vertical banner sin-margen-derecho con-margen-superior">
    @if (App\has_banner('single', 'banner-6'))
      <div class="centar-contenido d-none d-lg-flex">
        {!! App\get_banners('single', 'banner-6') !!}
      </div>
    @endif
    @if (App\has_banner('single', 'banner-7'))
      <div class="centar-contenido d-none d-lg-flex">
        {!! App\get_banners('single', 'banner-7') !!}
      </div>
    @endif
    @if (App\has_banner('single', 'banner-8'))
      <div class="centar-contenido">
        {!! App\get_banners('single', 'banner-8') !!}
      </div>
    @endif
    @if (App\has_banner('single', 'banner-9'))
      <div class="centar-contenido">
        {!! App\get_banners('single', 'banner-9') !!}
      </div>
    @endif
    @if (App\has_banner('single', 'banner-10'))
      <div class="centar-contenido">
        {!! App\get_banners('single', 'banner-10') !!}
      </div>
    @endif
    @if (App\has_banner('single', 'banner-11'))
      <div class="centar-contenido">
        {!! App\get_banners('single', 'banner-11') !!}
      </div>
    @endif
    @if (App\has_banner('single', 'banner-12'))
      <div class="centar-contenido">
        {!! App\get_banners('single', 'banner-12') !!}
      </div>
    @endif
    @if (App\has_banner('single', 'banner-13'))
      <div class="centar-contenido">
        {!! App\get_banners('single', 'banner-13') !!}
      </div>
    @endif
    @if (App\has_banner('single', 'banner-14'))
      <div class="centar-contenido">
        {!! App\get_banners('single', 'banner-14') !!}
      </div>
    @endif
    @if (App\has_banner('single', 'banner-15'))
      <div class="centar-contenido">
        {!! App\get_banners('single', 'banner-15') !!}
      </div>
    @endif
    @if (App\has_banner('single', 'banner-16'))
      <div class="centar-contenido">
        {!! App\get_banners('single', 'banner-16') !!}
      </div>
    @endif
  </div>
@endif