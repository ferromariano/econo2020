<div class="sidebar-right-container d-flex d-lg-none">
	@if (App\has_banner('single', 'banner-1'))
	  <div class="banner-vertical banner">
	    <div class="banner-vertical-rectangular centar-contenido">
	    	{!! App\get_banners('single', 'banner-1') !!}
	    </div>
	  </div>
	@endif
</div>
