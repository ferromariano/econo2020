<div class="noticia-social">
  <div class="nav-social-container">
    <div id="nav-social">
      <ul>
        <li>
          <a href="https://wa.me/?text={!! FrontPage::postTitleRaw() !!} <?php urlencode(the_permalink()); ?>" target="_blank">
            <span class="fa-stack" style="vertical-align: top;"><i class="fab fa-whatsapp fa-stack-2x"></i></span>
          </a>
        </li>
        <li>
          <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank">
            <span class="fa-stack" style="vertical-align: top;"><i class="far fa-circle fa-stack-2x"></i><i class="fab fa-facebook-f fa-stack-1x"></i></span>
          </a>
        </li>
        <li>
          <a href="https://twitter.com/share?text={!! FrontPage::postTitleRaw() !!}&url=<?php the_permalink(); ?>" target="_blank">
            <span class="fa-stack" style="vertical-align: top;"><i class="far fa-circle fa-stack-2x"></i><i class="fab fa-twitter fa-stack-1x"></i></span>
          </a>
        </li>
        <li>
          <a href="https://www.linkedin.com/shareArticle?url=<?php echo get_permalink(); ?>" target="_blank">
            <span class="fa-stack" style="vertical-align: top;"><i class="far fa-circle fa-stack-2x"></i><i class="fab fa-linkedin-in fa-stack-1x"></i></span>
          </a>
        </li>
        <li>
          <a href="javascript:window.print();">
            <span class="fa-stack" style="vertical-align: top;"><i class="far fa-circle fa-stack-2x"></i><i class="fa fa-print fa-stack-1x"></i></span>
          </a>
        </li>
      </ul>
    </div>
  </div>
</div>
