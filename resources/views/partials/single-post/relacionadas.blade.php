<section id="notas-listado">
  <div class="row">
    <div class="col-12 col-md-7 col-lg-8">
    	@while (SinglePost::havePost('relacionada'))
	      @include('partials.single-post.relacionada-item')
        @php
          SinglePost::resetPost()
        @endphp
    	@endwhile
    	@while (SinglePost::havePost('relacionada-cat'))
	      @include('partials.single-post.relacionada-item')
        @php
          SinglePost::resetPost()
        @endphp
    	@endwhile

    </div>
    <div class="col-12 col-md-5 col-lg-4">
      @include('partials.single-post.banners6')
    </div>
  </div>
</section>