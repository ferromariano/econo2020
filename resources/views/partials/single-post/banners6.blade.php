<div class="sidebar-right-container">
  @if (App\has_banners('single', array('banner-17','banner-18','banner-19','banner-20')))
    <div class="banner-vertical banner sin-margen-derecho">
      @if (App\has_banner('single', 'banner-17'))
        <div class="centar-contenido">
          {!! App\get_banners('single', 'banner-17') !!}
        </div>
      @endif
      @if (App\has_banner('single', 'banner-18'))
        <div class="centar-contenido">
          {!! App\get_banners('single', 'banner-18') !!}
        </div>
      @endif
      @if (App\has_banner('single', 'banner-19'))
        <div class="centar-contenido">
          {!! App\get_banners('single', 'banner-19') !!}
        </div>
      @endif
      @if (App\has_banner('single', 'banner-20'))
        <div class="centar-contenido">
          {!! App\get_banners('single', 'banner-20') !!}
        </div>
      @endif
    </div>
  @endif
</div>