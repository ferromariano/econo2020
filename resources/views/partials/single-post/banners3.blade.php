<div class="sidebar-right-container con-margen-inferior d-md-none">
  @if (App\has_banners('single', array('banner-2','banner-3','banner-4','banner-5')))
  <div class="banner-vertical banner sin-margen-derecho con-margen-superior">
    @if (App\has_banner('single', 'banner-2'))
    <div class="banner-vertical- centar-contenido">
      {!! App\get_banners('single', 'banner-2') !!}
    </div>
    @endif
    @if (App\has_banner('single', 'banner-3'))
    <div class="banner-vertical-rectangular centar-contenido">
      {!! App\get_banners('single', 'banner-3') !!}
    </div>
    @endif
    @if (App\has_banner('single', 'banner-4'))
    <div class="banner-vertical-rectangular centar-contenido">
      {!! App\get_banners('single', 'banner-4') !!}
    </div>
    @endif
    @if (App\has_banner('single', 'banner-5'))
    <div class="banner-vertical-rectangular centar-contenido">
      {!! App\get_banners('single', 'banner-5') !!}
    </div>
    @endif
  </div>
  @endif
</div>