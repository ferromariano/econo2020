@if (App\has_banners('category', array('banner-2', 'banner-3', 'banner-4')))
<div class="banner-vertical banner sin-margen-derecho">
  @if (App\has_banner('category', 'banner-2'))
  <div class="banner-vertical-1 centar-contenido d-none d-lg-flex">
    {!! App\get_banners('category', 'banner-2') !!}
  </div>
  @endif
  @if (App\has_banner('category', 'banner-3'))
  <div class="banner-vertical-2 centar-contenido">
    {!! App\get_banners('category', 'banner-3') !!}
  </div>
  @endif
  @if (App\has_banner('category', 'banner-4'))
  <div class="banner-vertical-3 centar-contenido">
    {!! App\get_banners('category', 'banner-4') !!}
  </div>
  @endif
</div>
@endif
