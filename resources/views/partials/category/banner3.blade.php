<aside class="d-none d-md-flex right-sidebar sin-margen-derecho">
  @include('partials.assets.mas-leidas')
</aside>

@if (App\has_banners('category', array('banner-5','banner-6','banner-7','banner-8')))
  <div class="banner-vertical banner sin-margen-derecho" style="max-height: 460px;">
    @if (App\has_banner('category', 'banner-5'))
    <div class="banner-vertical-1 centar-contenido">
      {!! App\get_banners('category', 'banner-5') !!}
    </div>
    @endif
    @if (App\has_banner('category', 'banner-6'))
    <div class="banner-vertical-2 centar-contenido">
      {!! App\get_banners('category', 'banner-6') !!}
    </div>
    @endif
    @if (App\has_banner('category', 'banner-7'))
    <div class="banner-vertical-3 centar-contenido">
      {!! App\get_banners('category', 'banner-7') !!}
    </div>
    @endif
    @if (App\has_banner('category', 'banner-8'))
    <div class="banner-vertical-4 centar-contenido">
      {!! App\get_banners('category', 'banner-8') !!}
    </div>
    @endif
  </div>
@endif