@if (Category::havePost('category'))
<article class="noticia con-margen-inferior">
  @if (Category::hasImagenHome())
  <div class="noticia-imagen">
    <a href="{!! Category::postLink() !!}">
      <figure>
        {!! Category::postImagenHome('545×364') !!}
      </figure>
    </a>
  </div>
  @endif
  <div class="noticia-volanta">{!! Category::postVolanta() !!}</div>
  <div class="noticia-titulo"><a href="{!! Category::postLink() !!}"><h3>{!! Category::postTitle() !!}</h3></a></div>
</article>
@php
  Category::resetPost()
@endphp
@endif
