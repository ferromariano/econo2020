@if (App\has_banners('category', array('wide-1', 'banner-1')))
<div class="row">
    <div class="col-12">
        @if (App\has_banner('category', 'wide-1'))
        <div class="row justify-content-center linea-separadora-superior-completa-negra d-none d-lg-flex">
            <div class="banner-horizontal banner">
              {!! App\get_banners('category', 'wide-1') !!}
            </div>
        </div>
        @endif
        @if (App\has_banner('category', 'banner-1'))
        <div class="row justify-content-center d-flex d-lg-none con-margen-inferior">
            <div class="banner-vertical-rectangular banner">
                {!! App\get_banners('category', 'banner-1') !!}
            </div>
        </div>
        @endif
    </div>
</div>
@endif