<div class="row">
  <div class="col-12 col-lg-8">
    <div class="row">
      <div class="col-12 col-md-8 col-lg-9 col-xl-8 linea-separadora-derecha-completa-gris">
        <div class="row">
          <div class="col-12">
            @if (Category::havePost('category'))
            <article class="noticia-margen-sidebar-izquierdo noticia noticia-destacada">
              <header>
                <div class="noticia-titulo"><a href="{!! Category::postLink() !!}"><h3>{!! Category::postTitle() !!}</h3></a></div>
              </header>
              <div class="noticia-texto texto-post-titulo">{!! Category::postContenido() !!}</div>
              <div class="noticia-imagen">
                <a href="{!! Category::postLink() !!}"><figure>{!! Category::postImagenHome('940×352') !!}</figure></a>
              </div>
            </article>
            @php
              Category::resetPost()
            @endphp
            @endif
          </div>
          <!--======BLOQUE INFERIOR======-->
          <div class="col-12 col-md-7 col-lg-8">
            <div class="noticia-margen-sidebar-izquierdo noticia-container linea-separadora-derecha-completa-gris">
              @if (Category::havePost('category'))
              <article class="noticia noticia-destacada">
                <div class="separator"></div>
                <header>
                  <div class="noticia-titulo"><a href="{!! Category::postLink() !!}"><h3>{!! Category::postTitle() !!}</h3></a></div>
                </header>
                <div class="noticia-texto">{!! Category::postContenido() !!}</div>
              </article>
              @php
                Category::resetPost()
              @endphp
              @endif
            </div>
          </div>
          <div class="col-12 col-md-5 col-lg-4">
            @if (Category::havePost('category'))
            <article class="noticia noticia-vertical ">
              <div class="separator"></div>
              <div class="noticia-imagen">
                <a href="{!! Category::postLink() !!}"><figure>{!! Category::postImagenHome('340×168') !!}</figure></a>
              </div>
              <header>
              <div class="noticia-titulo"><a href="{!! Category::postLink() !!}"><h3>{!! Category::postTitle() !!}</h3></a></div>
              </header>
            </article>
            @php
              Category::resetPost()
            @endphp
            @endif
          </div>
          <!--======BLOQUE INFERIOR===========-->
        </div>
      </div>
      <div class="col-12 col-md-4 col-lg-3 col-xl-4">
        @if (Category::havePost('category'))
        <article class="noticia noticia-vertical noticia-vertical-completa">
          <div class="d-none d-md-block noticia-imagen">
            <a href="{!! Category::postLink() !!}"><figure>{!! Category::postImagenHome('540×296') !!}</figure></a>
          </div>
          <header>
            <div class="noticia-volanta">{!! Category::postVolanta() !!}</div>
            <div class="noticia-titulo"><a href="{!! Category::postLink() !!}"><h3>{!! Category::postTitle() !!}</h3></a></div>
          </header>
          <div class="noticia-texto">{!! Category::postContenido() !!}</div>
          <div class="separator"></div>
        </article>
        @php
          Category::resetPost()
        @endphp
        @endif
        @if (Category::havePost('category'))
        <article class="noticia noticia-destacada anular-margen-izquierdo-992">
          <header>
            <div class="noticia-volanta">{!! Category::postVolanta() !!}</div>
            <div class="noticia-titulo"><a href="{!! Category::postLink() !!}"><h3>{!! Category::postTitle() !!}</h3></a></div>
          </header>
          <div class="noticia-texto">{!! Category::postContenido() !!}</div>
        </article>
        @php
          Category::resetPost()
        @endphp
        @endif
      </div>
    </div>
  </div>
  <div class="col-12 col-lg-4">
    @include('partials.category.banner2')
  </div>
</div>