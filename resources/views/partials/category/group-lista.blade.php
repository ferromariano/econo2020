<div class="col-12 col-lg-8">
  <div class="row noticias-seccion-especial-container fondo-blanco linea-separadora-inferior-completa-negra con-margen-inferior">
    <div class="col-12">
      <div class="row">
        <div class="col-6 col-md-3 linea-separadora-derecha-completa-gris">
          @include('partials.category.item')
        </div>
        <div class="col-6 col-md-3 linea-separadora-derecha-completa-gris">
          @include('partials.category.item')
        </div>
        <div class="col-6 col-md-3 linea-separadora-derecha-completa-gris">
          @include('partials.category.item')
        </div>
        <div class="col-6 col-md-3">
          @include('partials.category.item')
        </div>
      </div>
      <div class="row con-margen-superior">
        <div class="col-6 col-md-3 linea-separadora-derecha-completa-gris">
          @include('partials.category.item')
        </div>
        <div class="col-6 col-md-3 linea-separadora-derecha-completa-gris">
          @include('partials.category.item')
        </div>
        <div class="col-6 col-md-3 linea-separadora-derecha-completa-gris">
          @include('partials.category.item')
        </div>
        <div class="col-6 col-md-3">
          @include('partials.category.item')
        </div>
      </div>
      <div class="row con-margen-superior">
        <div class="col-6 col-md-3 linea-separadora-derecha-completa-gris">
          @include('partials.category.item')
        </div>
        <div class="col-6 col-md-3 linea-separadora-derecha-completa-gris">
          @include('partials.category.item')
        </div>
        <div class="col-6 col-md-3 linea-separadora-derecha-completa-gris">
          @include('partials.category.item')
        </div>
        <div class="col-6 col-md-3">
          @include('partials.category.item')
        </div>
      </div>
    </div>
  </div>
  <div class="row justify-content-center">
    @include('partials.category.paginado')
  </div>
</div>