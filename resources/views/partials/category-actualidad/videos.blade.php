@if (have_rows("slider_video", 'options'))
<section id="grupoVideosDestacados">
  <div class="sin-margen-izquierdo sin-margen-derecho linea-separadora-superior-completa-negra subtitulo-grupo-noticias-container">
    <div class="subtitulo-grupo-noticias">
      <img src="{!! App\asset_path('images/headlines_back_xl.jpeg') !!}" alt="headlines_back" title="headlines_back" />
      <div class="texto-centrado">
        <h2>Videos destacados</h2>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="row noticias-seccion-especial-container fondo-blanco linea-separadora-inferior-completa-negra con-margen-inferior">
        <div class="col-12">
          <div class="row">
            @while (have_rows("slider_video", 'options'))
            @php
            the_row(); 
            $code = get_sub_field('video');
            @endphp
            <div class="col-6 col-md-3 col-lg-2 linea-separadora-derecha-completa-gris">
              <article class="noticia con-margen-inferior">
                <div class="noticia-imagen">
                  <a href="https://www.youtube.com/watch?v=<?php echo $code ?>" data-toggle="modal" data-target="#video<?php echo md5($code); ?>">
                    <figure>
                      <img src="https://img.youtube.com/vi/<?php echo $code ?>/0.jpg" loading="lazy" />
                    </figure>
                  </a>
                </div>
                <div class="noticia-titulo">
                  <a href="https://www.youtube.com/watch?v=<?php echo $code ?>" data-toggle="modal" data-target="#video<?php echo md5($code); ?>">
                    <h3>@php the_sub_field('titulo') @endphp</h3>
                  </a>
                </div>
              </article>
            </div>
            @endwhile
          </div>
        </div>
      </div>
    </div>
  </div>

@while (have_rows("slider_video", 'options'))
  @php
  the_row(); 
  $code = get_sub_field('video')
  @endphp
  <div id="video<?php echo md5($code); ?>" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><?php the_sub_field('titulo'); ?></h4>
        </div>
        <div class="modal-body"><div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="//www.youtube.com/embed/<?php echo $code; ?>"></iframe></div></div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <script>;(function($) { $('#video<?php echo md5($code); ?>').on('hidden.bs.modal', function (e) { jQuery("#video<?php echo md5($code); ?> iframe").attr("src", jQuery("#video<?php echo md5($code); ?> iframe").attr("src")); })})(jQuery);</script>
@endwhile
</section>
@endif


