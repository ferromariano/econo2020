<form role="search" method="get" class="search-form" action="{!! esc_url( home_url( '/' ) ) !!}">
	<div class="search">
		<i class="fas fa-search"></i>
		<input type="search" class="search-header" placeholder="BUSCAR EN ECONOJOURNAL" value="{!! get_search_query() !!}">
	</div>
</form>
