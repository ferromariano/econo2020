<div id="widget-lasmasleidas">
  <div class="widget-title-container">
    <h2 class="widget-title">
      LAS MÁS LEÍDAS
    </h3>
  </div>
  @php
    $args = array(
      'limit' => 6
  );

  wpp_get_mostpopular($args);
  @endphp
</div>