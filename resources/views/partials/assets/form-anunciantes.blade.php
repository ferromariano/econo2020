  <section id="contacto-anunciantes" class="margen-superior-20">
    <div class="row">
      <div class="col">
        <div class="form-container sin-margen-izquierdo sin-margen-derecho">
          <div class="logo-social-container">
            <div class="form-logo-container">
              <a class="form-logo" href="index.html">
                <img class="logo" src="{!! App\asset_path('images/logo_formulario.png') !!}" alt="logo Econo Journal" title="logo Econo Journal">
              </a>
            </div>
            <div class="social-search-container">
              <div class="nav-social-container">
                <div id="nav-social-form-anunciantes">
                  <ul>
                    <li>
                      <a href="#" target="_blank">
                        <span class="fa-stack" style="vertical-align: top;">
                          <i class="fab fa-whatsapp fa-stack-2x"></i>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://web.facebook.com/econojournal/" target="_blank">
                        <span class="fa-stack" style="vertical-align: top;">
                          <i class="far fa-circle fa-stack-2x"></i>
                          <i class="fab fa-facebook-f fa-stack-1x"></i>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://www.instagram.com/econojournal/" target="_blank">
                        <span class="fa-stack" style="vertical-align: top;">
                          <i class="far fa-circle fa-stack-2x"></i>
                          <i class="fab fa-instagram fa-stack-1x"></i>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://www.youtube.com/channel/UCBbl2OHJCjOXh1yDQJ60ReQ" target="_blank">
                        <span class="fa-stack" style="vertical-align: top;">
                          <i class="far fa-circle fa-stack-2x"></i>
                          <i class="fab fa-youtube fa-stack-1x"></i>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://twitter.com/econojournal" target="_blank">
                        <span class="fa-stack" style="vertical-align: top;">
                          <i class="far fa-circle fa-stack-2x"></i>
                          <i class="fab fa-twitter fa-stack-1x"></i>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="https://ar.linkedin.com/company/econojournal.com.ar" target="_blank">
                        <span class="fa-stack" style="vertical-align: top;">
                          <i class="far fa-circle fa-stack-2x"></i>
                          <i class="fab fa-linkedin-in fa-stack-1x"></i>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="#" target="_blank">
                        <span class="fa-stack" style="vertical-align: top;">
                          <i class="far fa-circle fa-stack-2x"></i>
                          <i class="fab fa-skype fa-stack-1x"></i>
                        </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <h2>Contacto anunciantes</h2>
          <p>
            EconoJournal es una agencia de noticias con foco en la actualidad de la agenda energética.
            Los contenidos que se publican son producto de la investigación y del trabajo cotidiano de su equipo periodístico.
          </p>
          @php
            echo do_shortcode( '[contact-form-7 id="36871"]' );
          @endphp
      </div>
    </div>
  </div>
</section>