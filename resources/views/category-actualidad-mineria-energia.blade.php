@extends('layouts.app')

@section('content')

@include('partials.category.banner1')

<section id="grupoActualidad" class="margen-superior-20">
  <div class="sin-margen-izquierdo sin-margen-derecho linea-separadora-superior-completa-negra subtitulo-grupo-noticias-container">
    <div class="subtitulo-grupo-noticias">
      <img src="{!! App\asset_path('images/headlines_back.jpg') !!}" alt="headlines_back" title="headlines_back" />
      <div class="texto-centrado">
        <h2>{!! single_cat_title( '', false ) !!}</h2>
      </div>
    </div>
  </div>

  <div class="row">
  
    @include('partials.category.group-lista')

    <div class="col-12 col-lg-4">
      @include('partials.category.banner3')
    </div>
  </div>

  @if (App\has_banner('category', 'wide-3'))
  <div class="row justify-content-center linea-separadora-superior-completa-negra d-none d-lg-flex">
    <div class="banner-horizontal banner">
      {!! App\get_banners('category', 'wide-3') !!}
    </div>
  </div>
  @endif
</section>


@include('partials.category-actualidad.videos')

@include('partials.assets.form-anunciantes')

@endsection
