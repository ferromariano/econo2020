@extends('layouts.app')

@section('content')
<section>
	<header>
		<h1 class="text-center">Contenido no encontrado</h1>	
	</header>
	<div>
		{!! get_search_form(false) !!}
	</div>
	

</section>
@endsection
