export default {
  init() {
  },
  finalize() {

    if(jQuery('img[data-preloadimage]').length) {
      jQuery('img[data-preloadimage]').each(function() {
        $( this ).attr( 'src', $(this).data('preloadimage') );
      })
    }
    NavBarResponsive();

    var subir = $('.scroll-to-top');

    subir.click(function(e){
      e.preventDefault();
      ScrollToTop();
    });

    subir.removeClass('on');
    subir.addClass('off');

    $(window).scroll(ScrollPage);

    function ScrollPage(){
      var subir = $('.scroll-to-top');
      var scrollValue = $(this).scrollTop();
      if( scrollValue > 50 ) {
        subir.removeClass('off');
        subir.addClass('on');
      } else {
        subir.removeClass('on');
        subir.addClass('off');
      }
    }

    function ScrollToTop(){
      $('html, body').animate({scrollTop: 0}, 2000);
      var uri = window.location.toString();
      if (uri.indexOf('#') > 0) {
        var clean_uri = uri.substring(0, uri.indexOf('#'));
        window.history.replaceState({}, document.title, clean_uri);
      }
    }

    function NavBarResponsive(){
      var texto;
      texto = $('#HeaderWtiPetroleo td:first-child a').text();
      $( '#HeaderWtiPetroleo td:first-child' ).text(TextoNavBar(texto));
      texto = $('#HeaderBrentPetroleo td:first-child a').text();
      $( '#HeaderBrentPetroleo td:first-child' ).text(TextoNavBar(texto));
      texto = $('#HeaderGas td:first-child a').text();
      $( '#HeaderGas td:first-child' ).text(TextoNavBar(texto));
    }

    function TextoNavBar(texto) {
      var ancho = $( window ).width();
      if (ancho < 960){
        texto = texto.split(' ')[0];
      }
      return texto + ':';
    }



    function setCookie(cname, cvalue, exdays) { var d = new Date(); d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000)); var expires = 'expires='+d.toUTCString(); document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/'; }
    function getCookie(cname) { var name = cname + '='; var ca = document.cookie.split(';'); for(var i = 0; i < ca.length; i++) { var c = ca[i]; while (c.charAt(0) == ' ') { c = c.substring(1); } if (c.indexOf(name) == 0) { return c.substring(name.length, c.length); } } return ''; }
    function checkCookie(cname) { var v = getCookie(cname); if (v != '') { return true; } return false; }

    if(jQuery('div[data-popin]').length) {
      jQuery('div[data-popin]').each(function() {
        var $key = $( this ).data('popin');

        console.log($key);

        if(!checkCookie($key)) { 
          if (jQuery(window).outerHeight() > jQuery(window).outerWidth()) {
            $(this).find('img').css({ 'max-width':  '90vw', 'max-height': '90vh'  });
          } else {
            $(this).find('img').css({ 'max-width':  '90vw', 'max-height': '90vh' });
          }

          $(this).modal('show')
          $(this).on('hide.bs.modal', null, { key: $key }, function (e) { 
            console.log( e.data );
            setCookie(e.data.key, 1, 1); 
          });

          $(this).find('button').on('click', null, { modal: this }, function(e) {
            $(e.data.modal).data('bs.modal')._isTransitioning = false;
            $(e.data.modal).modal('hide');
          });
          $(this).find('a').on('click', null, { modal: this }, function(e) {
            $(e.data.modal).data('bs.modal')._isTransitioning = false;
            $(e.data.modal).modal('hide');
          });
        }
      });
    }

  },
};

