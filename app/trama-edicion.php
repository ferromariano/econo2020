<?php



add_action( 'init', function() {

	$labels = array(
		'name'              => _x( 'Ediciones', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Edicion', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Ediciones', 'textdomain' ),
		'all_items'         => __( 'All Ediciones', 'textdomain' ),
		'parent_item'       => __( 'Parent Edicion', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Edicion:', 'textdomain' ),
		'edit_item'         => __( 'Edit Edicion', 'textdomain' ),
		'update_item'       => __( 'Update Edicion', 'textdomain' ),
		'add_new_item'      => __( 'Add New Edicion', 'textdomain' ),
		'new_item_name'     => __( 'New Edicion Name', 'textdomain' ),
		'menu_name'         => __( 'Ediciones', 'textdomain' ),
	);

	$args = array(
		'labels'             => $labels,
		'description'        => 'Ediciones de revista trama',
		'public'             => true,
    "show_ui"            => true,
    "show_in_menu"       => true,
    "show_in_nav_menus"  => true,
    "query_var"          => true,
    "show_in_rest"       => true,
		// 'publicly_queryable' => false,
		// 'show_tagcloud'      => false,
		'rewrite'            => array( 'slug' => 'edicion' ),
		'hierarchical'       => true,
	  'show_in_quick_edit' => true,
		'show_admin_column'  => true,
	);


	register_taxonomy(
		'ediciones',
		'post',
		$args
	);	
}, 0 );


add_filter( 'pre_get_posts', 
  function($WP_Query ) {
  	if(is_admin())                    return;
  	if(!is_category('revista-trama')) return;
  	if(!$WP_Query->is_main_query())              return;

		$WP_Query->query_vars['orderby']  = array( 'meta_value_num' => 'DESC', 'date' => 'DESC' );
		$WP_Query->query_vars['meta_key'] = 'prioridad';

    $tax_query = array(
      'taxonomy' => 'ediciones',
      'terms'    => '',
      'operator' => 'EXISTS',
    );

    $WP_Query->tax_query->queries[] = $tax_query; 
    $WP_Query->query_vars['tax_query'] = $WP_Query->tax_query->queries;

  }
, 99, 1);





add_filter( 'posts_orderby', function ( $orderby, $wp_query ) {
	global $wpdb;
	if(is_admin())                    return $orderby;
	if(!is_category('revista-trama')) return $orderby;
	if(!$wp_query->is_main_query())              return $orderby;

	$orderby = "(
		SELECT GROUP_CONCAT($wpdb->terms.term_id ORDER BY $wpdb->terms.term_id ASC)
		FROM $wpdb->term_relationships
		INNER JOIN $wpdb->term_taxonomy USING (term_taxonomy_id)
		INNER JOIN $wpdb->terms USING (term_id)
		WHERE $wpdb->posts.ID = object_id
		AND $wpdb->term_taxonomy.taxonomy = 'ediciones'
		GROUP BY $wpdb->term_relationships.object_id
	) "
	. 'DESC, '
	. $orderby;

	return $orderby;
}, 10, 2 );


/**/










