<?php

namespace App;

/**
 * loadContenido
 * =====================
 *
 * @author      Mariano Ferro
 *
 * @version     2.0
 */
class loadContenido {
  static $templates  = array();
  static $filters    = array();  
  static $contenido  = array();  
  static $yaprint    = array();  
  static $debug      = true;
  static $debug_data = array();  
  static $context    = null;  

  function __construct() {

  }

  static function getIDs($slug=null) {
    $ids = array();
    foreach (self::$filters as $value) {
      foreach ($value['IDS'] as $k) {
        $ids[] = $k;
      }
    }
    return $ids;
  }

  public function addGroups($slug, $filter_config ) {
    self::$contenido[$slug] = new \WP_Query( $filter_config );

    self::$filters[$slug] = array(
      'filters'=> $filter_config,
      'have'   => true,
      'count'  => count(self::$contenido[$slug]->posts),
      'IDS'    => array()
    );

    foreach (self::$contenido[$slug]->posts as $p) {
      self::$filters[$slug]['IDS'][] = $p->ID;
    }

    self::addDebug($slug, 'Carga');
  }

  public static function addYaPrint($slug, $id) {
  	if (!isset(self::$yaprint[$slug])) {
  		self::$yaprint[$slug] = array();
  	}
    self::$yaprint[$slug][] = $id;
  }

  public static function hasAllsYaPrint($slug, $id) {

    foreach (self::$yaprint as $value) {
      if (in_array($id, $value)) {
        return true;
      }
    }

    return false;
  }


  public static function hasYaPrint($slug, $id) {
    if (!isset(self::$yaprint[$slug])) {
      self::$yaprint[$slug] = array();
    }
    return in_array($id, self::$yaprint[$slug]);
  }

  static public function hasSlug($slug) {
    return isset(self::$filters[$slug]) ? ( isset(self::$contenido[$slug]) ? true : false ) : false;
  }

  static public function getFilter($slug) {
		return self::$filters[$slug];
  }

  static public function getContenido($slug) {
  	return self::$contenido[$slug];
  }

  private function _prinTe($slug, $tipo, $banners_tipo = null, $vars=array(), $dir='') {


    if( self::$filters[$slug]['have'] == false ) { self::addDebug($slug, 'Sin datos'); return; }
    if( !self::$contenido[$slug]->have_posts() ) { self::addDebug($slug, 'Se termino los datos'); self::$filters[$slug]['have']=false; return; }
    
    self::$contenido[$slug]->the_post();

    if ($this->hasYaPrint($slug, get_the_ID()))  { 
      self::addDebug($slug, 'ya se imprimio - '.get_the_ID()); 
      $this->_prinTe($slug, $tipo, $banners_tipo, $vars, $dir);
      return; 
    }

    
    $this->addYaPrint($slug, get_the_ID());

    if (isset(self::$filters[$slug]['vars']['file-slug'])) {
      $fileSlug = self::$filters[$slug]['vars']['file-slug'];
    } elseif (isset($vars['file-slug'])) {
      $fileSlug = $vars['file-slug'];
    } else {
      $fileSlug = $slug;
    }     

//    echo '<div class="debug-layer">'.$fileSlug.'-'.$tipo.'</div>';

    get_template_part_and_vars($dir.'/'.$fileSlug, $tipo, $vars);
    self::addDebug($slug, 'Añade DIR: ' . $dir.'/'.$fileSlug .' | TIPO: '.$tipo . ' | POST_ID: '.self::$contenido[$slug]->post->ID );
    wp_reset_postdata();
  }

  static function loaddingCategoria() { 
    global $wp_query;
    $slug='category';

    self::$contenido[$slug] = $wp_query;

    self::$filters[$slug] = array(
      'filters'=> array(),
      'have'   => true,
      'count'  => count(self::$contenido[$slug]->posts),
      'IDS'    => array()
    );

    foreach (self::$contenido[$slug]->posts as $p) {
      self::$filters[$slug]['IDS'][] = $p->ID;
    }

    self::addDebug($slug, 'Carga');
    
  }

  static function loaddingRelacionadas($slug, $wp_query) { 

    self::$contenido[$slug] = $wp_query;

    self::$filters[$slug] = array(
      'filters'=> array(),
      'have'   => true,
      'count'  => count(self::$contenido[$slug]->posts),
      'IDS'    => array()
    );

    foreach (self::$contenido[$slug]->posts as $p) {
      self::$filters[$slug]['IDS'][] = $p->ID;
    }

    self::addDebug($slug, 'Carga');
    
  }



  private function setup_postdata($p) {
    global $post;
    $post = $p;
  }

  static function init() { 
    self::$context = new loadContenido();
  }

  static function getInstance() {
    if (!self::$context) {
      loadContenido::init();
    }
    return self::$context;
  }

  static function addDebug($slug, $m) {
    if (self::$debug) {
      self::$debug_data[] = time().' | '.$slug. ' >> ' .$m;
    }
  }

}