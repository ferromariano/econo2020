<?php

class InitACF
{

	public $path            = '../app/acf/';
	public $fileConfigPages = '_config-admin.php';
	public $files = [
		'page-config.php',
		'page-videos.php',
		'page-banners.php',
		'fields-category.php',
		'fields-post.php',
	];

	function __construct() {
		if( !function_exists('acf_add_local_field_group') ) return $this->errorACF('ACF Error: No se encuentran las funciones del plugin', true);
		if( !function_exists('acf_add_options_page') )      return $this->errorACF('ACF Error: No se encuentran las funciones del plugin', true);
		if( !function_exists('register_field_group') )      return $this->errorACF('ACF Error: No se encuentran las funciones del plugin', true);

		$this->configPagesACF();
		foreach ( $this->files as $file) { $this->loadFile( $this->path . $file ); }

	}

	private function configPagesACF() {
		$this->loadFile( $this->path . $this->fileConfigPages );
	}

	private function loadFile($file) {
		if (!$filepath = locate_template( $file )) $this->errorACF(sprintf(__('ACF Error: locating %s for inclusion', 'sage'), $file));
		require_once $filepath;		
	}

	function errorACF($e, $alerta=false) {
		if (!$alerta)
			trigger_error($e, E_USER_ERROR);

		echo '<h1>'.$e.'</h1>';
		return false;
	}
}

new InitACF();
