<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SinglePost extends Controller {

	use Partials\ContenidoTrait;

	public function __before() {

		$total = 5;
		$posts_id = get_field('relacionadas');

		if (!is_array($posts_id)) {
			$posts_id = array();
		}

		if ( (int) count($posts_id)) {
	    $attr = array( 'post_type'   => 'post',
	    	             'post__in'    => $posts_id,
	    	             'post_status' => 'publish' );

			\App\loadContenido::loaddingRelacionadas('relacionada', (new \WP_Query( $attr )));
		}

		$posts_id[] = get_the_ID();


		if (( (int) count($posts_id) ) < $total ) {

			$categories = get_the_category();

			if (count($categories)) {
				
				$resto = $total - ((int) count($posts_id));

	      $cats_id = array();

	      foreach ($categories as $v) { $cats_id[] = $v->term_id; }

	      $attr = array( 'cat'            => implode( ',', $cats_id), 
	                     'posts_per_page' => $resto,
	                     'post__not_in'   => $posts_id );


				\App\loadContenido::loaddingRelacionadas('relacionada-cat', (new \WP_Query( $attr )));

			}
		}
	}
}
