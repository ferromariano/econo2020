<?php

namespace App\Controllers\Partials;

trait ContenidoTrait
{

	public static function TramaUltimaEdicion() {
		$rs = get_terms(array('taxonomy' => 'ediciones', 'hide_empty' => false, 'orderby' => 'term_id', 'order' => 'DESC', 'number' => 1));
		return $rs[0];
	} 

	public static function resetPost() {
		wp_reset_postdata();
	}

	public static function havePost($slug, $printCheck=true, $allPrints=true) {
		if ( !\App\loadContenido::hasSlug($slug)             ) return false;

		if ( !(\App\loadContenido::getFilter($slug))['have'] ) return false;
		if ( !\App\loadContenido::getContenido($slug)->have_posts()) { 
			\App\loadContenido::$filters[$slug]['have'] = false;
			return false;
		}

		\App\loadContenido::getContenido($slug)->the_post();

		if ($printCheck) {
			if ($allPrints) {
				if (\App\loadContenido::hasAllsYaPrint($slug, get_the_ID())) {
					return self::havePost($slug, $printCheck);
				}
			} else {
				if (\App\loadContenido::hasYaPrint($slug, get_the_ID())) {
					return self::havePost($slug, $printCheck);
				}
			}
			\App\loadContenido::addYaPrint($slug, get_the_ID());
		}

		return true;
	}

	public static function hasImagenHome($id=null)      {
		return has_post_thumbnail();
	}	

	public static function postImagenHome($size, $id=null)      {
	  if ( !has_post_thumbnail() ) { return ''; }

	  if (is_string($size)) {
	    global $_wp_additional_image_sizes;
	    $n_size = 't'.$size;
	    if (isset($_wp_additional_image_sizes[$n_size])) {
	      $w = $_wp_additional_image_sizes[$n_size]['width'];
	      $h = $_wp_additional_image_sizes[$n_size]['height'];
	    } else {
	      $w = '100';
	      $h = '100';
	    }
	  } else {
	    $w = $size['width'];
	    $h = $size['height'];
	  }
	  $image        = \App\get_the_post_thumbnail_cdn( $id, $w, $h, 'BLUR');
	  $imagepreload = \App\get_the_post_thumbnail_cdn( $id, $w, $h, 90 );

	  return '<img data-preloadimage="'.$imagepreload.'" src="'. $image .'" />';
	}

	public static function postTaxos($id=null, $taxonomy='category')      {
		$tmp = get_the_terms(get_the_ID(), $taxonomy);
		
		if (!is_array($tmp) or count($tmp) <= 0) return array();
		$rs = array();
		foreach ( $tmp as $v) {
			$rs[] = array('n'=>$v->name, 'l'=> get_term_link($v->slug, $v->taxonomy) );
		}
		return $rs;
	}


	public static function postImagen($size)      {
		return '<img src="https://fakeimg.pl/'.$size.'" alt="'.$size.'">';
	}

	public static function postDate($id=null, $format='d/m/Y, H:i')      {
		return get_post_time($format, false, get_the_ID());
	}

	public static function postAutor($id=null)      {
		return 'Por '.( strlen( get_field('autor', $id) )  ? get_field('autor', $id) : get_the_author());
	}

	public static function postVolanta($id=null)      {
		return ( strlen( get_field('volanta', $id) )  ? get_field('volanta', $id) : '');;
	}

	public static function postLink($id=null)      {
		return get_permalink();
	}

	public static function postTitle($id=null)     {
		return get_the_title();
	}

	public static function postTitleRaw($id=null)     {
		return get_the_title();
	}

	public static function postCopete($id=null)     {
	  if( strlen(get_field('copete_home')) ) {
	    return get_field('copete_home');
	  }
	  return '';
	}


	public static function postContenido($id=null) {
	  if( strlen(get_field('copete_home')) ) {
	    return get_field('copete_home');
	  }

	  if( is_single() && !is_front_page() && !is_home() ) {
	    $post = get_post();
	    echo $post->post_excerpt;
	    return;
	  }

	  $text = get_the_content('');
	  $text = strip_shortcodes( $text );
	  $text = wp_trim_words( $text, 30, '' );

	  return $text;
	}

	public static function postImage2($size) {
	  $img_id = get_field('segunda_foto');


	  if ( !$img_id ) { return ''; }

	  if (is_string($size)) {
	    global $_wp_additional_image_sizes;
	    $n_size = 't'.$size;
	    if (isset($_wp_additional_image_sizes[$n_size])) {
	      $w = $_wp_additional_image_sizes[$n_size]['width'];
	      $h = $_wp_additional_image_sizes[$n_size]['height'];
	    } else {
	      $w = '100';
	      $h = '100';
	    }
	  } else {
	    $w = $size['width'];
	    $h = $size['height'];
	  }


	  $image        = \App\wp_get_attachment_url_cdn( $img_id, $w, $h, 'BLUR');
	  $imagepreload = \App\wp_get_attachment_url_cdn( $img_id, $w, $h, 90 );

	  return '<img data-preloadimage="'.$imagepreload.'" src="'. $image .'" />';

	}

	public static function postVolantaCompuesta($id=null)      {
		return (
						strlen( get_field('volanta_compuesta', $id) ) 
							? '<span class="resalte">'.get_field('volanta_compuesta', $id).'</span> /' 
							: ''
					 );
	}


}