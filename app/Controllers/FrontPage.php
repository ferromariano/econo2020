<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller {

	use Partials\ContenidoTrait;

	private $querys_frontend = array();

	public function __before() {
		$edicion = FrontPage::TramaUltimaEdicion();

		$this->querys_frontend = array(
			'destacada'  => array(
				'posts_per_page' => 4,
			),
			'oilgas' => array(
				'posts_per_page' => 15,
				'category_name'  => 'oilgas',
				'not_destacados' => true,
			),
			'energia' => array(
				'posts_per_page' => 16,
				'category_name'  => 'energia',
				'not_destacados' => true,
			),
			'renovables' => array(
				'posts_per_page' => 14,
				'category_name'  => 'renovables',
				'not_destacados' => true,
			),
			'actualidad' => array(
				'posts_per_page' => 15,
				'category_name'  => 'actualidad-mineria-energia',
				'not_destacados' => true,
			),
			'trama' => array( 
				'posts_per_page' => 4, 
				'category_name'  => 'revista-trama',
				'not_destacados' => true,
				'orderby'        => array( 'meta_value_num' => 'DESC', 'date' => 'DESC' ),
				'meta_key'       => 'prioridad', 
		    'tax_query' => array(
		        array(
				      'taxonomy' => 'ediciones',
				      'terms'    => $edicion->term_id,
				      'fields'   => 'term_id',
		        ),
		    ),
			),
			'opinion' => array(
				'posts_per_page' => 15,
				'category_name' => 'opinion-mineria-energia',
				'not_destacados' => true,
			),
			'mineria' => array(
				'posts_per_page' => 14,
				'category_name' => 'mineria',
				'not_destacados' => true,
			),
		);

		foreach ($this->querys_frontend as $key => $query) {
			if (!isset($query['post_type'])) $query['post_type'] = 'post';

			if (!isset($query['not_destacados'])) {
				$query['meta_key']		= 'tipo';
				$query['meta_value']	= '1';
//				$query['meta_query'] = array( 'key' => 'tipo', 'value' => '1', );
			} else {
				if (isset($query['category_name']) and $query['category_name'] == 'revista-trama') {
					# code...
				} else {
					$query['meta_key']		= 'tipo';
					$query['meta_value']	= '1';
					$query['meta_compare']	= '!=';

				} 
//					$query['meta_query'] = array( 'key' => 'tipo', 'value' => '1', 'compare' => '!=' );

				// if ($query['not_destacados'] != null) {
				// }


//				$query['meta_query'] = array( 'key' => 'tipo', 'value' => '0', );
			}

			\App\loadContenido::getInstance()->addGroups($key, $query);
		}

		$this->destacadosUpdate();
	}

	public function destacadosUpdate() {
		if (!isset($_GET['cron-destacados'])) return;


		$not_ids = array();

		while( FrontPage::havePost('destacada') ) {
			$not_ids[] = get_the_ID();

			FrontPage::resetPost();
		}

		var_dump($not_ids);

		$filter_config = array( 
		  'nopaging'       => true,
		  'post_type'      => 'post',
		  'meta_query'     => array( array( 'key' => 'tipo', 'value' => '1', ), ),
		  'post__not_in'   => $not_ids,
		);

		$rs = new \WP_Query( $filter_config );

		foreach ($rs->posts as $post) {
			var_dump($post->ID);
      update_post_meta( $post->ID, 'tipo', '0', '1' );
		}



		exit();
	}

}
