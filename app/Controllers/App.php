<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function __before() {
        \App\AdsManager::loadAds();
    }

    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function pageLinkNewsletter()   { return get_permalink( get_page_by_path( 'newsletter' ) ); }
    public static function pageLinkQuienesSomos() { return get_permalink( get_page_by_path( 'quienes-somos' ) ); }
    // public static function () { return get_permalink( get_page_by_path( 'map' ) ); }


    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf('Resultados de  %s', get_search_query());
        }
        if (is_404()) {
            return __('Contenido no encontrado', 'sage');
        }
        return get_the_title();
    }
}
