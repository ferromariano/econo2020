<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);


add_filter( 'pre_get_posts', 
    function($WP_Query ) {
        if(!$WP_Query->is_main_query())              return;

        if( ( is_search() || is_tag() ) && isset($_GET['date_limit']) && ( isset($WP_Query->query['s']) || isset($WP_Query->query['tag']) ) ) {
            if(!isset( $WP_Query->query_vars['date_query'] )) $WP_Query->query_vars['date_query'] = array();
            switch($_GET['date_limit']) {
                case '1': $WP_Query->query_vars['date_query']['after'] = '-1 day'; break;
                case '2': $WP_Query->query_vars['date_query']['after'] = '-1 month'; break;
                case '3': $WP_Query->query_vars['date_query']['after'] = '-1 year'; break;
            }
        }
    }
, 99, 1);

add_filter( 'pre_get_posts', 
  function($WP_Query ) {
    if(is_admin())                    return;
    if(!is_category('actualidad-mineria-energia')) return;
    if(!$WP_Query->is_main_query())              return;
    
    $WP_Query->set( 'posts_per_page', 12 );

  }
, 99, 1);


add_action( 'wp_ajax_search_tag',        'App\\json_search_callback' );
add_action( 'wp_ajax_nopriv_search_tag', 'App\\json_search_callback' );

function json_search_callback() {

    $taxonomy = sanitize_key( 'post_tag' );
    $tax = get_taxonomy( $taxonomy );
    if ( ! $tax ) {
        wp_die( json_encode(array()) );
    }

    if( !isset( $_REQUEST['tag_s'] ) ){
        wp_die( json_encode(array()) );
    }

    $s = wp_unslash( $_REQUEST['tag_s'] );

    $results = get_terms( $taxonomy, array( 'name__like' => $s, 'fields' => 'all', 'hide_empty' => false, 'number'=>15 ) );

    if( !count($results) ) {
        wp_die( json_encode(array()) );
    }

    $r = array();

    foreach ($results as $v) {
        $r[] = array (
            'link' => get_term_link($v),
            'text' => $v->name
        ); 
    }

    echo json_encode($r);

    wp_die();  
}


add_filter('wpp_custom_html', function($data, $attr) {
    $html = '';
    foreach ($data as $key => $post) {
        $html .= '<div class="widget-content">'
        . '<div class="position">'.($key+1).'.</div>'
        . '<div class="item-content">'
        . '<div class="headline-content">'
        . '<a href="'.get_permalink($post->id).'">'
        . '<h3>'
        . $post->title
        . '</h3>'
        . '</a>'
        . '</div>'
        . '<div class="separator"></div>'
        . '</div>'
        . '</div>';
    }
    return $html;
}, 100, 2);


function my_add_item( $admin_bar ){

  if (is_admin()) return;

  $view = false;
  $p_url = parse_url($_SERVER['REQUEST_URI']);
  if (isset($p_url['query'])) {
    if (stripos($p_url['query'], 'debugbanners') !== false) {

      $p_url['query'] = str_replace('debugbanners=1', '', $p_url['query']);
      $view = true;
    } else {
      $p_url['query'] .= '&debugbanners=1';
    }
  } else {
    $p_url['query'] = 'debugbanners=1';
  }
  $url = ($p_url['path'].'?'.$p_url['query'] );

  global $pagenow;
  $admin_bar->add_menu( 
    array(
      'id'    => 'cache-purge',
      'title' => ($view) ? 'Esconder info banner' : 'Ver Banners',
      'href'  => $url 
    ) 
  );
}

add_action('admin_bar_menu', 'App\\my_add_item', 100, 1);
