<?php

namespace App;

use Detection\Mobile_Detect;

/**
 * AdsManager
 */
class AdsManager
{
	static  $current=null;
	static  $MD=null;
	private $rows = array();
	static  $debug = false;


	function __construct($type, $attr=array()) {

		switch ($type) {
			case 'home':
				$rows = get_field('banners_home', 'options');
			break;

			case 'category':
				$rows = get_field('banners_category', 'options');
			break;

			case 'single':
				$rows = get_field('banners_single', 'options');
			break;

			case 'search':
				$rows = get_field('banners_buscador', 'options');

			break;
		}


		AdsManager::$MD = new \Mobile_Detect();

		$this->procesaData($rows, $type, $attr);
	}

	private function procesaData($rows, $type, $attr) {
		if (!is_array($rows)) return;

		foreach ($rows as $row) {
			if (!isset($row['slug'])     ) continue;
			if (strlen($row['slug']) <= 0) continue;

			if (!$row['enable'])           continue;

			if (AdsManager::$MD->isMobile()) {
				if($row['is_movil'] == 'no') continue;
			} else {
				if($row['is_movil'] == 'si') continue;
			}

			if ($row['hay_tiempo_de_campana']) {
        if (intval($row['desde']) > intval(date('Ymd'))) continue;
        if (intval($row['hasta']) < intval(date('Ymd'))) continue;
			}

			switch ($type) {
				case 'home':
				break;

				case 'category':
					if (!isset($attr['category_id']))              continue 2;
					if (!isset($row['categoria']))                 continue 2;
					if ($attr['category_id'] != $row['categoria']) continue 2;
				break;

				case 'single':
					$post_id = get_the_ID();
					$destacado = get_field('tipo', $post_id);
//					var_dump($destacado); exit();
					if ($destacado) {
						if (!$row['en_destacados']) {
							continue 2;
						}
					} else {
						if ($row['en_destacados']) { continue 2; }

						$cats = wp_get_post_categories($post_id, array('fields' => 'tt_ids'));

						if (is_array($cats)) {
							if (in_array('14', $cats)) {
								if (!is_array($row['categoria'])) continue 2;
								if (!in_array('14', $row['categoria'])) continue 2;
							}
						}

					}
				break;
				
				default:
					# code...
					break;
			}



			if (!isset($this->rows[ $row['slug'] ])) {
				$this->rows[ $row['slug'] ] = array();
			}

			$this->rows[ $row['slug'] ][] = $row;
		}
		
	}

  public function hasAction($sec, $pos) {

		if (AdsManager::$debug) {
			return true;
		}

		if (!isset($this->rows[$pos])) return false;
		if (!count($this->rows[$pos])) return false;

		return true;
  }

	public function printAction($sec, $pos) {

		if (AdsManager::$debug) {
			return $this->printActionDebug($pos);
		}

		if (!$this->hasAction($sec, $pos)) return '';

		$row = $this->rows[$pos][0];

		switch( $row['tipo'] ) {
			case 'imagen': return $this->printActionImagen($row); break;
			case 'js':     return $this->printActionJs($row);     break;
			case 'custom': return $this->printActionCustom($row); break;
			default:
				return '';
			break;
		}

	}


	public function printActionImagen($row) {
		return '<a href="'.$row['link'].'"><img src="'.$row['imagen'].'" loading="eager" /></a>';
	}

	public function printActionJs($row) {
		return $row['tag_javascript'];
	}

	public function printActionCustom($row) {
		return $row['custom_banner'];
	}

	public function printActionDebug($pos) {

		switch (substr($pos, 0, 4)) {
			case 'wide':
				return '<a class="banner_debug" href="#" style="width: 940px; height: 100px; background: #eb452e; display: block; text-align: center; color: #FFF; font-size: 30px; border: 5px solid #444; border-radius: 5px; line-height: 86px;"><div class="banner">'.$this->printActionDebugInfo($pos).'</div></a>';
			break;
			case 'bann':
				return '<a class="banner_debug" href="#"  style="width: 300px; height: 125px; background: #eb452e; display: block; text-align: center; color: #FFF; font-size: 30px; border: 5px solid #444; border-radius: 5px; line-height: 118px;"><div class="banner">'.$this->printActionDebugInfo($pos).'</div></a>';
			break;
			
			default:
				return '<a class="banner_debug" href="#"><div class="banner">'.$this->printActionDebugInfo($pos).'</div></a>';
			break;
		}
	}

	public function printActionDebugInfo($pos) {

		$html  = '<p style="font-size: 14px; line-height: 16px; text-align: left; padding: 10px; margin: 0;">';
		$html .= 'Posicion: <strong>'.$pos."</strong> | ";
		if (isset($this->rows[$pos])) {
			$html .= 'Tipo: <strong>'.$this->rows[$pos][0]['tipo']."</strong><br>";
			$html .= 'es movil: '.$this->rows[$pos][0]['is_movil']."<br>";
			$html .= 'Link: '.$this->rows[$pos][0]['link']."<br>";
			if (isset($this->rows[$pos][0]['desde']) and isset($this->rows[$pos][0]['hasta'])) {
			$html .= 'Desde: '.$this->rows[$pos][0]['desde']." | ";
			$html .= 'Hasta: '.$this->rows[$pos][0]['hasta']."<br>";
			}
		}
		$html .= '</p>';
		return $html;
	}


	static function print($sec, $pos) {
		if (self::$current == null) {
			return;
		}

		return (self::$current)->printAction($sec, $pos);

	}

	static function has($sec, $pos) {
		if (self::$current == null) {
			return;
		}

		return (self::$current)->printAction($sec, $pos);

	}


	static function loadAds() {
		if (self::$current != null) {
			return;
		}

		if (isset($_GET['debugbanners'])) {
			AdsManager::$debug = true;
		} elseif (defined('BANNERS_DEBUG')) {
			AdsManager::$debug = true;
		}


		if (is_front_page()) {
			self::$current = new AdsManager('home');
		} elseif (is_category()) {
			$term = get_queried_object();
			self::$current = new AdsManager('category', array('category_id' => $term->term_id));
		} elseif (is_singular('post')) {
			self::$current = new AdsManager('single');
		} elseif (is_search() or is_tag()) {
			self::$current = new AdsManager('search');
		}
	}

}