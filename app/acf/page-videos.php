<?php

acf_add_local_field_group(array (
	'key'      => 'group_577f22d58b54d',
	'title'    => 'Slider home',
	'location' => array ( array ( array ( 'param' => 'options_page', 'operator' => '==', 'value' => 'page_slider_video', ), ), ),
	'fields'   => array (
		array (
			'key'               => 'field_577f22e4a1e80',
			'label'             => 'slider_video',
			'name'              => 'slider_video',
			'type'              => 'repeater',
			'value'             => NULL,
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array ( 'width' => '', 'class' => '', 'id' => '', ),
			'min'               => 0,
			'max'               => 0,
			'layout'            => 'row',
			'button_label'      => 'Añadir Video',
			'collapsed'         => '',
			'sub_fields' => array (
				array ( 'key' => 'field_577f25d06a032', 'label' => 'Titulo', 'name' => 'titulo', 'type' => 'text', 'value' => NULL, 'instructions' => '',                                                                                                                        'required' => 0, 'conditional_logic' => 0, 'wrapper' => array ( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'prepend' => '', 'append' => '', 'maxlength' => '', 'readonly' => 0, 'disabled' => 0, ),
				array ( 'key' => 'field_577f22eda1e81', 'label' => 'Vídeo',  'name' => 'video',  'type' => 'text', 'value' => NULL, 'instructions' => 'Ej: si esta fuera la url del video https://www.youtube.com/watch?v=L7sllWyZ5Sg tendrian que ingrezar solo "L7sllWyZ5Sg"', 'required' => 1, 'conditional_logic' => 0, 'wrapper' => array ( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'prepend' => '', 'append' => '', 'maxlength' => '', 'readonly' => 0, 'disabled' => 0, ),
			),
		),
	),
	'menu_order'            => 0,
	'position'              => 'normal',
	'style'                 => 'default',
	'label_placement'       => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen'        => '',
	'active'                => 1,
	'description'           => '',
));
