<?php

$zonas_noticias = array( 
	'banner-0'  => 'banner-0', 
	'banner-1'  => 'banner-1', 
	'banner-2'  => 'banner-2', 
	'banner-3'  => 'banner-3', 
	'banner-4'  => 'banner-4', 
	'banner-5'  => 'banner-5', 
	'banner-6'  => 'banner-6', 
	'banner-7'  => 'banner-7', 
	'banner-8'  => 'banner-8', 
	'banner-9'  => 'banner-9', 
	'banner-10' => 'banner-10', 
	'banner-11' => 'banner-11', 
	'banner-12' => 'banner-12', 
	'banner-13' => 'banner-13', 
	'banner-14' => 'banner-14', 
	'banner-15' => 'banner-15', 
	'banner-16' => 'banner-16', 
	'banner-17' => 'banner-17',   
	'banner-18' => 'banner-18',   
	'banner-19' => 'banner-19',   
	'banner-20' => 'banner-20',   
	'wide-1'    => 'wide-1', 
	'wide-2'    => 'wide-2', 
	'wide-3'    => 'wide-3', 
);



acf_add_local_field_group(array(
	'key' => 'group_5f0b6b48f0bf8',
	'title' => 'HOME',
	'fields' => array(
		array(
			'key' => 'field_5f0b6b93cee60',
			'label' => 'Banners en la home',
			'name' => 'banners_home',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => 'field_5f0b6bcacee61',
			'min' => 0,
			'max' => 0,
			'layout' => 'row',
			'button_label' => 'Añadir banner',
			'sub_fields' => array(
				array( 'key' => 'field_5f0b6bcacee61', 'label' => 'Posicion del banner', 'name' => 'slug', 'type' => 'select', 'instructions' => '', 'required' => 1, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'choices' => array( 'wide-1' => 'wide-1', 'wide-2' => 'wide-2', 'wide-3' => 'wide-3', 'banner-0' => 'banner-0', 'banner-1' => 'banner-1', 'banner-2' => 'banner-2', 'banner-3' => 'banner-3', 'banner-4' => 'banner-4', 'banner-5' => 'banner-5', 'banner-6' => 'banner-6', 'banner-7' => 'banner-7', 'banner-8' => 'banner-8', 'banner-9' => 'banner-9', 'banner-10' => 'banner-10', 'banner-11' => 'banner-11', 'banner-12' => 'banner-12', 'banner-13' => 'banner-13', 'banner-14' => 'banner-14', 'banner-15' => 'banner-15', 'banner-16' => 'banner-16', 'banner-17' => 'banner-17', ), 'default_value' => array( ), 'allow_null' => 0, 'multiple' => 0, 'ui' => 0, 'return_format' => 'value', 'ajax' => 0, 'placeholder' => '', ),
				array( 'key' => 'field_5f0b740dc5530', 'label' => 'habilitado', 'name' => 'enable', 'type' => 'true_false', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '', 'default_value' => 1, 'ui' => 0, 'ui_on_text' => '', 'ui_off_text' => '', ),
				array( 'key' => 'field_5f0b6cd1cee62', 'label' => 'Tipo de banner', 'name' => 'tipo', 'type' => 'select', 'instructions' => '', 'required' => 1, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'choices' => array( 'imagen' => 'imagen', 'js' => 'js', 'custom' => 'custom', ), 'default_value' => array( ), 'allow_null' => 0, 'multiple' => 0, 'ui' => 0, 'return_format' => 'value', 'ajax' => 0, 'placeholder' => '', ),
				array( 'key' => 'field_5f0b86bbe4692', 'label' => 'es movil', 'name' => 'is_movil', 'type' => 'radio', 'instructions' => '', 'required' => 1, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'choices' => array( 'si' => 'si', 'no' => 'no', 'any' => 'any', ), 'allow_null' => 0, 'other_choice' => 0, 'default_value' => 'any', 'layout' => 'horizontal', 'return_format' => 'value', 'save_other_choice' => 0, ),
				array( 'key' => 'field_5f0b721c15e69', 'label' => '', 'name' => '', 'type' => 'message', 'instructions' => '', 'required' => 0, 'conditional_logic' => array( array( array( 'field' => 'field_5f0b6cd1cee62', 'operator' => '==', 'value' => 'imagen', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '<h3>Configurar banner de imagen</h3>', 'new_lines' => '', 'esc_html' => 0, ),
				array( 'key' => 'field_5f0b6e6c13c77', 'label' => 'Imagen del banner', 'name' => 'imagen', 'type' => 'image', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0b6cd1cee62', 'operator' => '==', 'value' => 'imagen', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'return_format' => 'url', 'preview_size' => 'full', 'library' => 'uploadedTo', 'min_width' => '', 'min_height' => '', 'min_size' => '', 'max_width' => '', 'max_height' => '', 'max_size' => '', 'mime_types' => '', ),
				array( 'key' => 'field_5f0b6eda13c78', 'label' => 'Link del banner', 'name' => 'link', 'type' => 'text', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0b6cd1cee62', 'operator' => '==', 'value' => 'imagen', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'prepend' => '', 'append' => '', 'maxlength' => '', ),
				array( 'key' => 'field_5f0b72b2924cf', 'label' => '', 'name' => '', 'type' => 'message', 'instructions' => '', 'required' => 0, 'conditional_logic' => array( array( array( 'field' => 'field_5f0b6cd1cee62', 'operator' => '==', 'value' => 'js', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '<h3>Configurar banner de JavaScript</h3>', 'new_lines' => '', 'esc_html' => 0, ),
				array( 'key' => 'field_5f0b6efc13c79', 'label' => 'Tag Javascript', 'name' => 'tag_javascript', 'type' => 'textarea', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0b6cd1cee62', 'operator' => '==', 'value' => 'js', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'maxlength' => '', 'rows' => 6, 'new_lines' => '', ),
				array( 'key' => 'field_5f0b6f1b13c7a', 'label' => 'Custom banner', 'name' => 'custom_banner', 'type' => 'textarea', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0b6cd1cee62', 'operator' => '==', 'value' => 'custom', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'maxlength' => '', 'rows' => '', 'new_lines' => '', ),
				array( 'key' => 'field_5f0b6e0313c76', 'label' => 'Tag Print', 'name' => 'tag_print', 'type' => 'text', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'prepend' => '', 'append' => '', 'maxlength' => '', ),
				array( 'key' => 'field_5f0b72d5924d0', 'label' => '', 'name' => '', 'type' => 'message', 'instructions' => '', 'required' => 0, 'conditional_logic' => array( array( array( 'field' => 'field_5f0b6cd1cee62', 'operator' => '==', 'value' => 'imagen', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '<h3>Configurar fecha</h3>', 'new_lines' => '', 'esc_html' => 0, ),
				array( 'key' => 'field_5f0b6f9513c7b', 'label' => 'Hay tiempo de campaña', 'name' => 'hay_tiempo_de_campana', 'type' => 'true_false', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '', 'default_value' => 0, 'ui' => 0, 'ui_on_text' => '', 'ui_off_text' => '', ),
				array( 'key' => 'field_5f0b6fe713c7c', 'label' => 'Desde', 'name' => 'desde', 'type' => 'date_picker', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0b6f9513c7b', 'operator' => '==', 'value' => '1', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'display_format' => 'F j, Y', 'return_format' => 'Ymd', 'first_day' => 1, ),
				array( 'key' => 'field_5f0b704913c7d', 'label' => 'Hasta', 'name' => 'hasta', 'type' => 'date_picker', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0b6f9513c7b', 'operator' => '==', 'value' => '1', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'display_format' => 'F j, Y', 'return_format' => 'Ymd', 'first_day' => 1, ),
			),
		),
 	),
 	'location' => array(
	 	array(
	 		array(
 				'param' => 'options_page',
 				'operator' => '==',
 				'value' => 'page_danners',
 			),
 		),
 	),
 	'menu_order' => 0,
 	'position' => 'normal',
 	'style' => 'default',
 	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));






























acf_add_local_field_group(array(
	'key' => 'group_5f0b6Categoria',
	'title' => 'Categoria',
	'fields' => array(
		array(
			'key'   => 'field_5f0bb9a82a954',
			'label' => 'Banners en la category',
			'name'  => 'banners_category',
			'type'  => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => 'field_5f0bb9a82a958',
			'min' => 0,
			'max' => 0,
			'layout' => 'row',
			'button_label' => 'Añadir banner',
			'sub_fields' => array(
				array( 'key' => 'field_5f0bb9a82a958', 'label' => 'Posicion del banner', 'name' => 'slug', 'type' => 'select', 'instructions' => '', 'required' => 1, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'choices' => array( 'banner-1' => 'banner-1', 'banner-2' => 'banner-2', 'banner-3' => 'banner-3', 'banner-4' => 'banner-4', 'banner-5' => 'banner-5', 'banner-6' => 'banner-6', 'banner-7' => 'banner-7', 'banner-8' => 'banner-8', 'wide-1' => 'wide-1', 'wide-2' => 'wide-2', 'wide-3' => 'wide-3', ), 'default_value' => array( ), 'allow_null' => 0, 'multiple' => 0, 'ui' => 0, 'return_format' => 'value', 'ajax' => 0, 'placeholder' => '', ), 
				array( 'key' => 'field_5f0bb9c92a964', 'label' => 'Categoria', 'name' => 'categoria', 'type' => 'taxonomy', 'instructions' => '', 'required' => 1, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'taxonomy' => 'category', 'field_type' => 'select', 'allow_null' => 1, 'add_term' => 0, 'save_terms' => 0, 'load_terms' => 0, 'return_format' => 'id', 'multiple' => 0, ), 
				array( 'key' => 'field_5f0bb9a82a956', 'label' => 'habilitado', 'name' => 'enable', 'type' => 'true_false', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '', 'default_value' => 1, 'ui' => 0, 'ui_on_text' => '', 'ui_off_text' => '', ), 
				array( 'key' => 'field_5f0bb9a82a955', 'label' => 'Tipo de banner', 'name' => 'tipo', 'type' => 'select', 'instructions' => '', 'required' => 1, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'choices' => array( 'imagen' => 'imagen', 'js' => 'js', 'custom' => 'custom', ), 'default_value' => array( ), 'allow_null' => 0, 'multiple' => 0, 'ui' => 0, 'ajax' => 0, 'return_format' => 'value', 'placeholder' => '', ), 
				array( 'key' => 'field_5f0bb9a82a957', 'label' => 'es movil', 'name' => 'is_movil', 'type' => 'radio', 'instructions' => '', 'required' => 1, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'choices' => array( 'si' => 'si', 'no' => 'no', 'any' => 'any', ), 'allow_null' => 0, 'other_choice' => 0, 'save_other_choice' => 0, 'default_value' => 'any', 'layout' => 'horizontal', 'return_format' => 'value', ), 
				array( 'key' => 'field_5f0bb9a82a959', 'label' => '', 'name' => '', 'type' => 'message', 'instructions' => '', 'required' => 0, 'conditional_logic' => array( array( array( 'field' => 'field_5f0bb9a82a955', 'operator' => '==', 'value' => 'imagen', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '<h3>Configurar banner de imagen</h3>', 'new_lines' => '', 'esc_html' => 0, ), 
				array( 'key' => 'field_5f0bb9a82a95a', 'label' => 'Imagen del banner', 'name' => 'imagen', 'type' => 'image', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0bb9a82a955', 'operator' => '==', 'value' => 'imagen', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'return_format' => 'url', 'preview_size' => 'full', 'library' => 'uploadedTo', 'min_width' => '', 'min_height' => '', 'min_size' => '', 'max_width' => '', 'max_height' => '', 'max_size' => '', 'mime_types' => '', ), 
				array( 'key' => 'field_5f0bb9a82a95b', 'label' => 'Link del banner', 'name' => 'link', 'type' => 'text', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0bb9a82a955', 'operator' => '==', 'value' => 'imagen', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'prepend' => '', 'append' => '', 'maxlength' => '', ), 
				array( 'key' => 'field_5f0bb9a82a95c', 'label' => '', 'name' => '', 'type' => 'message', 'instructions' => '', 'required' => 0, 'conditional_logic' => array( array( array( 'field' => 'field_5f0bb9a82a955', 'operator' => '==', 'value' => 'js', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '<h3>Configurar banner de JavaScript</h3>', 'new_lines' => '', 'esc_html' => 0, ), 
				array( 'key' => 'field_5f0bb9a82a95d', 'label' => 'Tag Javascript', 'name' => 'tag_javascript', 'type' => 'textarea', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0bb9a82a955', 'operator' => '==', 'value' => 'js', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'maxlength' => '', 'rows' => 6, 'new_lines' => '', ), 
				array( 'key' => 'field_5f0bb9a82a95e', 'label' => 'Custom banner', 'name' => 'custom_banner', 'type' => 'textarea', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0bb9a82a955', 'operator' => '==', 'value' => 'custom', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'maxlength' => '', 'rows' => '', 'new_lines' => '', ), 
				array( 'key' => 'field_5f0bb9a82a95f', 'label' => 'Tag Print', 'name' => 'tag_print', 'type' => 'text', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'prepend' => '', 'append' => '', 'maxlength' => '', ), 
				array( 'key' => 'field_5f0bb9a82a960', 'label' => '', 'name' => '', 'type' => 'message', 'instructions' => '', 'required' => 0, 'conditional_logic' => array( array( array( 'field' => 'field_5f0bb9a82a955', 'operator' => '==', 'value' => 'imagen', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '<h3>Configurar fecha</h3>', 'new_lines' => '', 'esc_html' => 0, ), 
				array( 'key' => 'field_5f0bb9a82a961', 'label' => 'Hay tiempo de campaña', 'name' => 'hay_tiempo_de_campana', 'type' => 'true_false', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '', 'default_value' => 0, 'ui' => 0, 'ui_on_text' => '', 'ui_off_text' => '', ), 
				array( 'key' => 'field_5f0bb9a82a962', 'label' => 'Desde', 'name' => 'desde', 'type' => 'date_picker', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0bb9a82a961', 'operator' => '==', 'value' => '1', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'display_format' => 'F j, Y', 'return_format' => 'Ymd', 'first_day' => 1, ), 
				array( 'key' => 'field_5f0bb9a82a963', 'label' => 'Hasta', 'name' => 'hasta', 'type' => 'date_picker', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0bb9a82a961', 'operator' => '==', 'value' => '1', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'display_format' => 'F j, Y', 'return_format' => 'Ymd', 'first_day' => 1, ),
			),
		),
 	),
 	'location' => array(
	 	array(
	 		array(
 				'param' => 'options_page',
 				'operator' => '==',
 				'value' => 'page_danners',
 			),
 		),
 	),
 	'menu_order' => 0,
 	'position' => 'normal',
 	'style' => 'default',
 	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));



































acf_add_local_field_group(array(
	'key' => 'group_5f0bNoticias',
	'title' => 'Noticias',
	'fields' => array(
		array(
			'key' => 'field_5f0d5a7766105',
			'label' => 'Banners en Noticia',
			'name' => 'banners_single',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => 'field_5f0d5a7766106',
			'min' => 0,
			'max' => 0,
			'layout' => 'row',
			'button_label' => 'Añadir banner',
			'sub_fields' => array(
				array( 'key' => 'field_5f0d5a7766106', 'label' => 'Posicion del banner', 'name' => 'slug', 'type' => 'select', 'instructions' => '', 'required' => 1, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'choices' => $zonas_noticias, 'default_value' => array( ), 'allow_null' => 0, 'multiple' => 0, 'ui' => 0, 'ajax' => 0, 'return_format' => 'value', 'placeholder' => '', ), 
				array( 'key' => 'field_5f0d5a7766108', 'label' => 'habilitado', 'name' => 'enable', 'type' => 'true_false', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '', 'default_value' => 1, 'ui' => 0, 'ui_on_text' => '', 'ui_off_text' => '', ), 
				array( 'key' => 'field_5f0d5a7766107', 'label' => 'Categoria', 'name' => 'categoria', 'type' => 'taxonomy', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'taxonomy' => 'category', 'field_type' => 'multi_select', 'allow_null' => 1, 'add_term' => 0, 'save_terms' => 0, 'load_terms' => 0, 'return_format' => 'id', 'multiple' => 0, ), 
				array( 'key' => 'field_5f0d5ad866116', 'label' => 'en destacados', 'name' => 'en_destacados', 'type' => 'true_false', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '', 'default_value' => 0, 'ui' => 0, 'ui_on_text' => '', 'ui_off_text' => '', ), 
				array( 'key' => 'field_5f0d5a7766109', 'label' => 'Tipo de banner', 'name' => 'tipo', 'type' => 'select', 'instructions' => '', 'required' => 1, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'choices' => array( 'imagen' => 'imagen', 'js' => 'js', 'custom' => 'custom', ), 'default_value' => array( ), 'allow_null' => 0, 'multiple' => 0, 'ui' => 0, 'ajax' => 0, 'return_format' => 'value', 'placeholder' => '', ), 
				array( 'key' => 'field_5f0d5a776610a', 'label' => 'es movil', 'name' => 'is_movil', 'type' => 'radio', 'instructions' => '', 'required' => 1, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'choices' => array( 'si' => 'si', 'no' => 'no', 'any' => 'any', ), 'allow_null' => 0, 'other_choice' => 0, 'save_other_choice' => 0, 'default_value' => 'any', 'layout' => 'horizontal', 'return_format' => 'value', ), 
				array( 'key' => 'field_5f0d5a776610b', 'label' => '', 'name' => '', 'type' => 'message', 'instructions' => '', 'required' => 0, 'conditional_logic' => array( array( array( 'field' => 'field_5f0d5a7766109', 'operator' => '==', 'value' => 'imagen', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '<h3>Configurar banner de imagen</h3>', 'new_lines' => '', 'esc_html' => 0, ), 
				array( 'key' => 'field_5f0d5a776610c', 'label' => 'Imagen del banner', 'name' => 'imagen', 'type' => 'image', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0d5a7766109', 'operator' => '==', 'value' => 'imagen', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'return_format' => 'url', 'preview_size' => 'full', 'library' => 'uploadedTo', 'min_width' => '', 'min_height' => '', 'min_size' => '', 'max_width' => '', 'max_height' => '', 'max_size' => '', 'mime_types' => '', ), 
				array( 'key' => 'field_5f0d5a776610d', 'label' => 'Link del banner', 'name' => 'link', 'type' => 'text', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0d5a7766109', 'operator' => '==', 'value' => 'imagen', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'prepend' => '', 'append' => '', 'maxlength' => '', ), 
				array( 'key' => 'field_5f0d5a776610e', 'label' => '', 'name' => '', 'type' => 'message', 'instructions' => '', 'required' => 0, 'conditional_logic' => array( array( array( 'field' => 'field_5f0d5a7766109', 'operator' => '==', 'value' => 'js', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '<h3>Configurar banner de JavaScript</h3>', 'new_lines' => '', 'esc_html' => 0, ), 
				array( 'key' => 'field_5f0d5a776610f', 'label' => 'Tag Javascript', 'name' => 'tag_javascript', 'type' => 'textarea', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0d5a7766109', 'operator' => '==', 'value' => 'js', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'maxlength' => '', 'rows' => 6, 'new_lines' => '', ), 
				array( 'key' => 'field_5f0d5a7766110', 'label' => 'Custom banner', 'name' => 'custom_banner', 'type' => 'textarea', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0d5a7766109', 'operator' => '==', 'value' => 'custom', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'maxlength' => '', 'rows' => '', 'new_lines' => '', ), 
				array( 'key' => 'field_5f0d5a7766111', 'label' => 'Tag Print', 'name' => 'tag_print', 'type' => 'text', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'prepend' => '', 'append' => '', 'maxlength' => '', ), 
				array( 'key' => 'field_5f0d5a7766112', 'label' => '', 'name' => '', 'type' => 'message', 'instructions' => '', 'required' => 0, 'conditional_logic' => array( array( array( 'field' => 'field_5f0d5a7766109', 'operator' => '==', 'value' => 'imagen', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '<h3>Configurar fecha</h3>', 'new_lines' => '', 'esc_html' => 0, ), 
				array( 'key' => 'field_5f0d5a7766113', 'label' => 'Hay tiempo de campaña', 'name' => 'hay_tiempo_de_campana', 'type' => 'true_false', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '', 'default_value' => 0, 'ui' => 0, 'ui_on_text' => '', 'ui_off_text' => '', ), 
				array( 'key' => 'field_5f0d5a7766114', 'label' => 'Desde', 'name' => 'desde', 'type' => 'date_picker', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0d5a7766113', 'operator' => '==', 'value' => '1', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'display_format' => 'F j, Y', 'return_format' => 'Ymd', 'first_day' => 1, ), 
				array( 'key' => 'field_5f0d5a7766115', 'label' => 'Hasta', 'name' => 'hasta', 'type' => 'date_picker', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f0d5a7766113', 'operator' => '==', 'value' => '1', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'display_format' => 'F j, Y', 'return_format' => 'Ymd', 'first_day' => 1, ),
 			),
 		),
 	),
 	'location' => array(
	 	array(
	 		array(
 				'param' => 'options_page',
 				'operator' => '==',
 				'value' => 'page_danners',
 			),
 		),
 	),
 	'menu_order' => 0,
 	'position' => 'normal',
 	'style' => 'default',
 	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));






















acf_add_local_field_group(array(
	'key' => 'group_5f0b6bBuscador',
	'title' => 'Buscador',
	'fields' => array(
 		array( 			'key' => 'field_5f305c6c24d31',
			'label' => 'Banners en buscador',
			'name' => 'banners_buscador',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => 'field_5f305c6c24d32',
			'min' => 0,
			'max' => 0,
			'layout' => 'row',
			'button_label' => 'Añadir banner',
			'sub_fields' => array(
				array( 'key' => 'field_5f305c6c24d32', 'label' => 'Posicion del banner', 'name' => 'slug', 'type' => 'select', 'instructions' => '', 'required' => 1, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'choices' => array( 'wide-1' => 'wide-1', 'wide-2' => 'wide-2', 'banner-0' => 'banner-0', 'banner-1' => 'banner-1', 'banner-2' => 'banner-2', 'banner-3' => 'banner-3', 'banner-4' => 'banner-4', 'banner-5' => 'banner-5', 'banner-6' => 'banner-6', ), 'default_value' => array( ), 'allow_null' => 0, 'multiple' => 0, 'ui' => 0, 'return_format' => 'value', 'ajax' => 0, 'placeholder' => '', ), 
				array( 'key' => 'field_5f305c6c24d33', 'label' => 'habilitado', 'name' => 'enable', 'type' => 'true_false', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '', 'default_value' => 1, 'ui' => 0, 'ui_on_text' => '', 'ui_off_text' => '', ), 
				array( 'key' => 'field_5f305c6c24d34', 'label' => 'Tipo de banner', 'name' => 'tipo', 'type' => 'select', 'instructions' => '', 'required' => 1, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'choices' => array( 'imagen' => 'imagen', 'js' => 'js', 'custom' => 'custom', ), 'default_value' => array( ), 'allow_null' => 0, 'multiple' => 0, 'ui' => 0, 'ajax' => 0, 'return_format' => 'value', 'placeholder' => '', ), 
				array( 'key' => 'field_5f305c6c24d35', 'label' => 'es movil', 'name' => 'is_movil', 'type' => 'radio', 'instructions' => '', 'required' => 1, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'choices' => array( 'si' => 'si', 'no' => 'no', 'any' => 'any', ), 'allow_null' => 0, 'other_choice' => 0, 'save_other_choice' => 0, 'default_value' => 'any', 'layout' => 'horizontal', 'return_format' => 'value', ), 
				array( 'key' => 'field_5f305c6c24d36', 'label' => '', 'name' => '', 'type' => 'message', 'instructions' => '', 'required' => 0, 'conditional_logic' => array( array( array( 'field' => 'field_5f305c6c24d34', 'operator' => '==', 'value' => 'imagen', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '<h3>Configurar banner de imagen</h3>', 'new_lines' => '', 'esc_html' => 0, ), 
				array( 'key' => 'field_5f305c6c24d37', 'label' => 'Imagen del banner', 'name' => 'imagen', 'type' => 'image', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f305c6c24d34', 'operator' => '==', 'value' => 'imagen', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'return_format' => 'url', 'preview_size' => 'full', 'library' => 'uploadedTo', 'min_width' => '', 'min_height' => '', 'min_size' => '', 'max_width' => '', 'max_height' => '', 'max_size' => '', 'mime_types' => '', ), 
				array( 'key' => 'field_5f305c6c24d38', 'label' => 'Link del banner', 'name' => 'link', 'type' => 'text', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f305c6c24d34', 'operator' => '==', 'value' => 'imagen', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'prepend' => '', 'append' => '', 'maxlength' => '', ), 
				array( 'key' => 'field_5f305c6c24d39', 'label' => '', 'name' => '', 'type' => 'message', 'instructions' => '', 'required' => 0, 'conditional_logic' => array( array( array( 'field' => 'field_5f305c6c24d34', 'operator' => '==', 'value' => 'js', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '<h3>Configurar banner de JavaScript</h3>', 'new_lines' => '', 'esc_html' => 0, ), 
				array( 'key' => 'field_5f305c6c24d3a', 'label' => 'Tag Javascript', 'name' => 'tag_javascript', 'type' => 'textarea', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f305c6c24d34', 'operator' => '==', 'value' => 'js', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'maxlength' => '', 'rows' => 6, 'new_lines' => '', ), 
				array( 'key' => 'field_5f305c6c24d3b', 'label' => 'Custom banner', 'name' => 'custom_banner', 'type' => 'textarea', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f305c6c24d34', 'operator' => '==', 'value' => 'custom', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'maxlength' => '', 'rows' => '', 'new_lines' => '', ), 
				array( 'key' => 'field_5f305c6c24d3c', 'label' => 'Tag Print', 'name' => 'tag_print', 'type' => 'text', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'prepend' => '', 'append' => '', 'maxlength' => '', ), 
				array( 'key' => 'field_5f305c6c24d3d', 'label' => '', 'name' => '', 'type' => 'message', 'instructions' => '', 'required' => 0, 'conditional_logic' => array( array( array( 'field' => 'field_5f305c6c24d34', 'operator' => '==', 'value' => 'imagen', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '<h3>Configurar fecha</h3>', 'new_lines' => '', 'esc_html' => 0, ), 
				array( 'key' => 'field_5f305c6c24d3e', 'label' => 'Hay tiempo de campaña', 'name' => 'hay_tiempo_de_campana', 'type' => 'true_false', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '', 'default_value' => 0, 'ui' => 0, 'ui_on_text' => '', 'ui_off_text' => '', ), 
				array( 'key' => 'field_5f305c6c24d3f', 'label' => 'Desde', 'name' => 'desde', 'type' => 'date_picker', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f305c6c24d3e', 'operator' => '==', 'value' => '1', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'display_format' => 'F j, Y', 'return_format' => 'Ymd', 'first_day' => 1, ), 
				array( 'key' => 'field_5f305c6c24d40', 'label' => 'Hasta', 'name' => 'hasta', 'type' => 'date_picker', 'instructions' => '', 'required' => 1, 'conditional_logic' => array( array( array( 'field' => 'field_5f305c6c24d3e', 'operator' => '==', 'value' => '1', ), ), ), 'wrapper' => array( 'width' => '', 'class' => '', 'id' => '', ), 'display_format' => 'F j, Y', 'return_format' => 'Ymd', 'first_day' => 1, ),
			),
 		),
 	),
 	'location' => array(
	 	array(
	 		array(
 				'param' => 'options_page',
 				'operator' => '==',
 				'value' => 'page_danners',
 			),
 		),
 	),
 	'menu_order' => 0,
 	'position' => 'normal',
 	'style' => 'default',
 	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));
