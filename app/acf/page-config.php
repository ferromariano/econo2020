<?php 

/**
 * 
 * Ultimo momento
 *
 */
register_field_group(array (
  'key' => 'group_551ff9c603696',
  'title' => 'Ultimo momento',
  'fields' => array (
    array ( 'key' => 'field_gh3234gvj245j', 'label' => '1 Ultimo momento', 'name' => '1_ultimo_momento', 'prefix' => '', 'type' => 'true_false', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'message' => '', 'default_value' => 0, ),
    array ( 'key' => 'field_551ff9cec17ae', 'label' => '2 noticias home', 'name' => 'ultimo_momento', 'prefix' => '', 'type' => 'true_false', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'message' => '', 'default_value' => 0, ),
  ),
  'location' => array (
    array (
      array (
        'param' => 'options_page',
        'operator' => '==',
        'value' => 'page_config',
      ),
    ),
  ),
  'menu_order' => 0,
  'position' => 'side',
  'style' => 'seamless',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => '',
));



/**
 * 
 * Video en vivo
 *
 */
acf_add_local_field_group(array (
  'key'    => 'group_58c2624620e5f',
  'title'  => 'Video en VIVO',
  'fields' => array (
    array ( 'key' => 'field_58c2626a0a7c8', 'label' => 'Hay video en vivo', 'name' => 'is_video_en_vivo', 'type' => 'true_false', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'wrapper' => array ( 'width' => '', 'class' => '', 'id' => '', ), 'message' => '', 'default_value' => 0, 'ui' => 0, 'ui_on_text' => '', 'ui_off_text' => '',     ),
    array ( 'key' => 'field_5h4j36vghk24s', 'label' => 'Titulo',            'name' => 'titulo_en_vivo', 'type' => 'text', 'instructions' => '', 'required' => 0, 'conditional_logic' => array ( array ( array ( 'field' => 'field_58c2626a0a7c8', 'operator' => '==', 'value' => '1', ), ), ), 'wrapper' => array ( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'prepend' => '', 'append' => '', 'maxlength' => '', ),
    array ( 'key' => 'field_5h4jd235da5s5', 'label' => 'Bajada',            'name' => 'descripcion_en_vivo', 'type' => 'text', 'instructions' => '', 'required' => 0, 'conditional_logic' => array ( array ( array ( 'field' => 'field_58c2626a0a7c8', 'operator' => '==', 'value' => '1', ), ), ), 'wrapper' => array ( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'prepend' => '', 'append' => '', 'maxlength' => '', ),
    array ( 'key' => 'field_58c262990a7c9', 'label' => 'codigo enbed',      'name' => 'video_en_vivo', 'type' => 'text', 'instructions' => '', 'required' => 0, 'conditional_logic' => array ( array ( array ( 'field' => 'field_58c2626a0a7c8', 'operator' => '==', 'value' => '1', ), ), ), 'wrapper' => array ( 'width' => '', 'class' => '', 'id' => '', ), 'default_value' => '', 'placeholder' => '', 'prepend' => '', 'append' => '', 'maxlength' => '', ),
  ),
  'location' => array ( array ( array ( 'param' => 'options_page', 'operator' => '==', 'value' => 'page_config', ), ), ),
  'menu_order' => 0,
  'position' => 'side',
  'style' => 'default',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => '',
  'active' => 1,
  'description' => '',
));
