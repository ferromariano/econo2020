<?php 

/**
 * 
 * Personalizacion de Post/Noticias
 *
 */


register_field_group(array (
  'key'                   => 'group_551fd64c0a024',
  'title'                 => 'Configuraciones',
  'menu_order'            => 0,
  'position'              => 'acf_after_title',
  'style'                 => 'default',
  'label_placement'       => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen'        => '',
  'location'              => array ( array ( array ( 'param' => 'post_type', 'operator' => '==', 'value' => 'post', ), ), ),
  'fields' => array (
    array ( 'key' => 'field_5519ce9b78948', 'label' => '¿ Es destacada ?', 'name' => 'tipo',         'prefix' => '', 'type' => 'true_false',   'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'message' => '', 'default_value' => 0, ),
    array ( 'key' => 'field_5520230a6464f', 'label' => 'Volanta',          'name' => 'volanta',      'prefix' => '', 'type' => 'text', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'default_value' => '', 'placeholder' => '', 'prepend' => '', 'append' => '', 'maxlength' => '', 'readonly' => 0, 'disabled' => 0 ),
    array ( 'key' => 'field_5519cf0478949', 'label' => 'Titulo corto',     'name' => 'titulo_home',  'prefix' => '', 'type' => 'text', 'instructions' => 'Titutlo corto', 'required' => 0, 'conditional_logic' => 0, 'default_value' => '', 'placeholder' => '', 'prepend' => '', 'append' => '', 'formatting' => 'html', 'maxlength' => '', 'readonly' => 0, 'disabled' => 0, ),
    array ( 'key' => 'field_5519d3110e159', 'label' => 'Copete corto',     'name' => 'copete_home',  'prefix' => '', 'type' => 'textarea', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'default_value' => '', 'placeholder' => '', 'maxlength' => '', 'rows' => 4, 'new_lines' => 'br', 'readonly' => 0, 'disabled' => 0, ),
    array ( 'key' => 'field_551fd676e70ca', 'label' => 'Relacionadas',     'name' => 'relacionadas', 'prefix' => '', 'type' => 'relationship', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'post_type' => array ( 0 => 'post', ), 'taxonomy' => '', 'filters' => array ( 0 => 'search', 1 => 'taxonomy', ), 'elements' => array ( 0 => 'featured_image', ), 'max' => 6, 'return_format' => 'id', ),
  ),
));


acf_add_local_field_group(array(
  'key' => 'group_5f2b3b4e808fd',
  'title' => 'Revista Trama',
  'fields' => array(
    array(
      'key' => 'field_5f2b4bc816fd2',
      'label' => 'segunda foto',
      'name' => 'segunda_foto',
      'type' => 'image',
      'instructions' => 'Tamaño 200×406',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'return_format' => 'id',
      'preview_size' => 'full',
      'library' => 'all',
      'min_width' => '',
      'min_height' => '',
      'min_size' => '',
      'max_width' => '',
      'max_height' => '',
      'max_size' => '',
      'mime_types' => '',
    ),
    array(
      'key' => 'field_5f2b4c6b16fd3',
      'label' => 'Volanta compuesta',
      'name' => 'volanta_compuesta',
      'type' => 'text',
      'instructions' => 'volanta compuesta, texto rojo en las noticias destacadas',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => '',
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
    ),
    array(
      'key' => 'field_5f2b4cc716fd4',
      'label' => 'prioridad',
      'name' => 'prioridad',
      'type' => 'text',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'default_value' => 1,
      'placeholder' => '',
      'prepend' => '',
      'append' => '',
      'maxlength' => '',
    ),
  ),
  'location' => array(
    array(
      array(
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'post',
      ),
      array(
        'param' => 'post_category',
        'operator' => '==',
        'value' => 'category:revista-trama',
      ),
    ),
  ),
  'menu_order' => 0,
  'position' => 'normal',
  'style' => 'default',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => '',
  'active' => true,
  'description' => '',
));

