<?php 

/**
 * 
 * Personalizacion de Categorias
 *
 */

register_field_group(array (
  'key' => 'group_551a2fab8adce',
  'title' => 'config categoria',
  'fields' => array (
    array ( 'key' => 'field_5519f7171dfa8', 'label' => 'color', 'name' => 'color', 'prefix' => '', 'type' => 'color_picker', 'instructions' => '', 'required' => 0, 'conditional_logic' => 0, 'default_value' => '#FFF', ),
  ),
  'location' => array ( array ( array ( 'param' => 'taxonomy', 'operator' => '==', 'value' => 'category', ), ), ),
  'menu_order' => 0,
  'position' => 'normal',
  'style' => 'seamless',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => array (
  ),
));
