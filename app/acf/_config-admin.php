<?php 

acf_add_options_page(array(
  'page_title'  => 'Configuracion',
  'menu_title'  => 'Configuracion',
  'menu_slug'   => 'page_config',
  'capability'  => 'edit_posts',
  'parent_slug' => '',
  'position'    => 1,
  'icon_url'    => false,
  'redirect'    => true
));

acf_add_options_page(array(
  'page_title'  => 'Slider Videos',
  'menu_title'  => 'Videos',
  'menu_slug'   => 'page_slider_video',
  'capability'  => 'edit_posts',
  'parent_slug' => '',
  'position'    => 5,
  'icon_url'    => false,
  'redirect'    => true
));  

acf_add_options_page(array(
  'page_title'  => 'Ads',
  'menu_title'  => 'Ads Banners',
  'menu_slug'   => 'page_danners',
  'capability'  => 'edit_posts',
  'parent_slug' => '',
  'position'    => 99,
  'icon_url'    => false,
  'redirect'    => true
));  




acf_add_options_sub_page('General');
